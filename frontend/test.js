import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { render, screen, waitFor }  from '@testing-library/react';
import axios from 'axios';

import "@testing-library/jest-dom/extend-expect";
import 'regenerator-runtime/runtime';

import Home from "./src/components/Home.jsx"
import About from "./src/components/About.jsx";
import Disaster from "./src/components/Disaster.jsx";
import Country from "./src/components/Country.jsx";
import Organization from "./src/components/Organization.jsx";

configure({ adapter: new Adapter() });
jest.mock('axios');

var user_data = {
    "overall_stats": {
      "commits": 161,
      "issues": 54
    },
    "user_table": [
      {
        "bio": "Hey I’m Mei! I’m a second year CS major interested in surgical robotics. When I’m not busy with CS, I like to boulder at ABP, play soccer, and hang out with friends. ",
        "commits": 22,
        "issues": 13,
        "name": "Mei Lin",
        "resp": "Front-End Developer",
        "unitTests": "5",
        "username": "mei-f-lin"
      },
      {
        "bio": "Yiheng is currently a junior at UT Austin and an aspiring Data Scientists. He’s responsible for implementing the backend database, API, and web application and ensures that the frontend has what it needs to display. In his spare time, Yiheng loves to hit the local arcades and play rhyme games such as Dance Dance Revolution. ",
        "commits": 84,
        "issues": 8,
        "name": "Yiheng Su",
        "resp": "Full Stack Developer | Phase II Leader",
        "unitTests": "6",
        "username": "Sam.Su1"
      },
      {
        "bio": "I am a junior computer science major at UT Austin. I am from Houston, TX, and in my free time I like to binge Netflix or hang out with my dance team.",
        "commits": 31,
        "issues": 20,
        "name": "Ashish Kanjiram",
        "resp": "Front-End Developer | Phase I Leader",
        "unitTests": "5",
        "username": "akanjiram"
      },
      {
        "bio": "Tejas is a Junior at UT Austin. He has some experience with web development from his previous internship. Some of his hobbies include programming and playing chess.",
        "commits": 19,
        "issues": 4,
        "name": "Tejas Karuturi",
        "resp": "Front-End Developer",
        "unitTests": "5",
        "username": "tkaruturi"
      },
      {
        "bio": "Anvith is a sophomore at UT. He is responsiblefor some of the front-end work that needs to be done. In his free time he enjoys playing basketball and watching tv.",
        "commits": 5,
        "issues": 9,
        "name": "anvithpotluri",
        "resp": "Front-End Developer",
        "unitTests": "5",
        "username": "anvithpotluri"
      }
    ]
};

describe('Trivial Truth Test', () => {
    test('true is truthy', () => {
        expect(true).toBe(true);
    });
});

describe('Environments', () => {
    test('API Link', () => {
        expect(process.env.REACT_APP_API_URL).toBe("https://disaster-averted.me/api");
    });
});



describe('Render Tests', () => {
    test('Home renders', () => {
        render(<Home />);
    });

    test('About renders', async() => {
        axios.get.mockResolvedValue({ data: user_data });
        render(<About />);
        expect(screen.getByText('Team Stats')).toBeInTheDocument();
    });

    test('Disaster renders', () => {
        render(<Disaster />);
    });

    test('Country renders', () => {
        render(<Country />);
    });

    test('Organization renders', () => {
        render(<Organization />);
    });

});

describe("Pages", () => {
    test("Home/Splash page", () => {
        const homeTest = shallow(<Home />)
        expect(homeTest).toMatchSnapshot()
    });

    test("About page", async() => {
        const aboutTest = shallow(<About />)
        expect(aboutTest).toMatchSnapshot()
    });
});

describe("Models", () => {
    test("Disaster Model page", () => {
        const aboutTest = shallow(<Disaster />)
        expect(aboutTest).toMatchSnapshot()
    });

    test("Country Model page", () => {
        const aboutTest = shallow(<Country />)
        expect(aboutTest).toMatchSnapshot()
    });

    test("Organization Model page", () => {
        const aboutTest = shallow(<Organization />)
        expect(aboutTest).toMatchSnapshot()
    });
});
