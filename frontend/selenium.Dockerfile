FROM python
WORKDIR /frontend

RUN pip install webdriver-manager
RUN pip install selenium
CMD bash

RUN pip freeze | grep webdriver-manager
RUN pip freeze | grep selenium

RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y apt-utils
RUN curl -sL https://deb.nodesource.com/setup_15.x | bash
RUN apt-get install -y nodejs
RUN npm -v
RUN node -v
