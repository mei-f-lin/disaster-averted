import React, { Component } from "react";
import {Card, Container, CardDeck, Alert, Row, Col} from 'react-bootstrap';
import axios from 'axios';
import MapContainer from './MapsAPI';
import Typography from '@material-ui/core/Typography';


class Country extends Component {
  constructor(props) {
    // since we are extending class Table so we have to use super in order to override Component class constructor
    super(props)
    this.state = {
      attributes: ["Status", "Income Level", "Area (sq. km)", "Population", "Disaster", "Longitude", "Latitude", "Calling Code", "Capital", "Region", "Subregion", "Time Zones", "Country ISO ID", "Income ISO ID", "Organization", "Flag", "Disaster"],
      id: 0,
      name: "",
      status: "",
      alerts: ['danger', 'warning', 'primary'],
      alertType: -1,
      income: "",
      area: 0,
      population: 0,
      longitude: 0,
      latitude: 0,
      callingCodes: "",
      capital: "",
      region: "",
      subregion: "",
      timezones: "",
      iso3: "",
      income_iso2: "",
      flag: "", 
      org_id: 0,
      disaster_id: 0,
      
      //for organization card
      org_attributes: ["Name", "Type", "Headquarters", "Status", 
      "Job Openings", "Training Opportunities", "Wrote Reports", "Homepage"],
      organization: "",
      logo_url: "",
      has_job: "",
      has_training: "",
      has_report: "",

      //for disaster card
      disaster_attributes: ["Name", "Type", "Day", "Month", "Year"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      images: ["https://media3.s-nbcnews.com/j/newscms/2020_35/3407271/200825-california-lnu-fire-jm-0957_c9b8238c91004412d2f816fdff345a93.fit-1240w.jpg", "https://www.redzone.co/wp-content/uploads/2018/04/river-flood-1030x687.jpg", "https://www.redrisk.com/redeng/Projects/projects/project12/topimage.jpg", "https://iadsb.tmgrup.com.tr/ed03ed/0/0/0/0/800/518?u=https://idsb.tmgrup.com.tr/2019/01/31/arctic-cold-wave-sends-temps-to-record-lows-in-us-midwest-expected-to-ease-1548943614299.jpg", "https://media.tzuchi.us/wp-content/uploads/2018/11/09152516/ecuadorearthquake2016-4.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBhnM-GByo4Gvz7MurvqxD1Z2vIf7EZ6VezA&usqp=CAU"],
      disaster: "",
      type_id: "",
      num: -1,
      type: "",
      day: 0,
      month: 0,
      year: 0,
    }
  }
  componentDidMount() {
    this.fetchCountryData()
  }

  fetchCountryData = async () => {
    var link = window.location.href
    var pieces = link.split("/")
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/countries/` + pieces[pieces.length - 1],
    ); 
    const { area, callingCodes, capital, flag, id, income, income_iso2, iso3, latitude, longitude, population, region, status, subregion, timezones } = data; //objects stores area, calling codes, etc.
    var { name } = data;
    this.setState({ area, callingCodes, capital, flag, id, income, income_iso2, iso3, latitude, longitude, name, population, region, status, subregion, timezones});
    if(this.state.status === "alert") {
      this.state.alertType = 0;
    } else if(this.state.status === "current") {
      this.state.alertType = 1;
    } else {
      this.state.alertType = 2;
    }
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/countries_link/` + this.state.id,
    );
    const { disaster_id, org_id } = data;
    this.setState({ disaster_id, org_id });
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/disasters/` + this.state.disaster_id,
    );
    var { name, type_id, type, day, month, year } = data;
    this.setState({ disaster: name, type_id, type, day, month, year });
    var num2 = 0;
    if (type_id === "WF") {
      num2 = 0;
    }
    else if (type_id === "FL"){
      num2 = 1;
    }
    else if(type_id === "FF"){
      num2 = 1;
    }
    else if(type_id === "AC"){
      num2 = 2;
    }
    else if(type_id === "CW"){
      num2 = 3;
    }
    else if(type_id === "EQ"){
      num2 = 4;
    }
    else{
      num2 = 5;
    }
    this.state.num = num2;
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/orgs/` + this.state.org_id,
    );
    const { s_name, logo_url, has_job, has_training, has_report, homepage } = data;
    this.setState({ organization: s_name, logo_url, has_job, has_training, has_report, homepage });
  }

  render() {
    return (
      <div className="Country">
        <header className="Country-header my-5">
          <h1>{this.state.name}</h1>
          <MapContainer></MapContainer>
          <img 
            alt = ""
            src = {this.state.flag} 
            height = {228}
            style={{
              position: 'relative',
              left: '-20%',
              right: '0%',
            }}
          />
        </header>
        <body class="App">
            <center>
              <Alert variant={this.state.alerts[this.state.alertType]}>
                <Alert.Heading>{this.state.attributes[0]}</Alert.Heading>
                <p className="mb-0">
                  {this.state.status}
                </p>
              </Alert>
              <div>
              <Container>
                <Row className='my-5'>
                    <Col>
                      <h5><b>{this.state.attributes[8]}</b></h5>
                      <h5>{this.state.capital}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[9]}</b></h5>
                      <h5>{this.state.region}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[10]}</b></h5>
                      <h5>{this.state.subregion}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[1]}</b></h5>
                      <h5>{this.state.income}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[2]}</b></h5>
                      <h5>{this.state.area}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[3]}</b></h5>
                      <h5>{this.state.population}</h5>
                    </Col>
                    
                </Row>
                <Row className='my-5'>
                    <Col>
                      <h5><b>{this.state.attributes[5]}</b></h5>
                      <h5>{this.state.longitude}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[6]}</b></h5>
                      <h5>{this.state.latitude}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[7]}</b></h5>
                      <h5>{this.state.callingCodes}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[11]}</b></h5>
                      <h5>{this.state.timezones}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[12]}</b></h5>
                      <h5>{this.state.iso3}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[13]}</b></h5>
                      <h5>{this.state.income_iso2}</h5>
                    </Col>
                </Row>
              </Container>
              </div>
              <CardDeck style={{ width: '80%' }}>
              <Card>
                <a style={{ cursor: 'pointer', color: 'black'}} href ={"/organizations/" + this.state.org_id}>
                <Card.Img variant="top" src={this.state.logo_url} height={260} />
                <Card.Body>
                  <Card.Title>
                  <b>{this.state.attributes[14]}: {this.state.organization}</b>
                  </Card.Title>
                  <Typography>
                    {this.state.org_attributes[7]}: <a href= {this.state.homepage}>{this.state.homepage}</a>
                  </Typography>
                  <Typography>
                    {this.state.org_attributes[3]}: {this.state.status}
                  </Typography>
                  <Typography>
                    {this.state.org_attributes[4]}: {this.state.has_job}
                  </Typography>
                  <Typography>
                    {this.state.org_attributes[5]}: {this.state.has_training}
                  </Typography>
                  <Typography>
                    {this.state.org_attributes[6]}: {this.state.has_report}
                  </Typography>
                </Card.Body>
                </a>
              </Card>
              
              <Card>
              <a style={{ cursor: 'pointer', color: 'black'}} href ={"/disasters/" + this.state.disaster_id}>
                <Card.Img variant="top" src = {this.state.images[this.state.num]} height={260} />
                <Card.Body>
                  <Card.Title>
                    <b>{this.state.attributes[16]}: {this.state.disaster}</b>
                  </Card.Title>
                  <Typography>
                    {this.state.disaster_attributes[1]}: {this.state.type}
                  </Typography>
                  <Typography>
                    {this.state.disaster_attributes[2]}: {this.state.day}
                  </Typography>
                  <Typography>
                    {this.state.disaster_attributes[3]}: {this.state.months[this.state.month - 1]}
                  </Typography>
                  <Typography>
                    {this.state.disaster_attributes[4]}: {this.state.year}
                  </Typography>
                </Card.Body>
                </a>
              </Card>
            </CardDeck>
            <br></br>
          </center>
        </body>
      </div>
    );
  }
}

export default Country;