import React, { Component } from "react";
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import Highlighter from "react-highlight-words";
import './bars.css';
import './ModelPage.css';
import {
  FormGroup,
  FormLabel,
  TextField,
  Checkbox,
  FormControlLabel,
} from '@material-ui/core';


class Countries extends Component {
  constructor(props) {
    super(props)
    this.state = {
      header: [
        {
          name: "id", 
          label:"ID", 
          options:{
            filter: false, 
            sort: false,
            display: false
          }
        }, 
        {
          name: "name", 
          label:"Name",
          options: {
            filter: false,
            /* enable highlighting */
            customBodyRender: this.customRender
          }
        }, 
        {
          name: "status", 
          label:"Status",
          options: {
            hint: "Whether the country has an active situation",
            filterType: 'multiselect',
            customFilterListOptions: {
              render: v => {return "Status: " + v}
            },
            /* enable highlighting */
            customBodyRender: this.customRender
          }
        }, 
        {
          name: "income", 
          label:"Income",
          options: {
            filterType: 'multiselect',
            customFilterListOptions: {
              render: v => {return "Income: " + v}
            },
            /* enable highlighting */
            customBodyRender: this.customRender
          }
        }, 
        {
          name: "area", 
          label:"Area (sq. km)",
          options:{
            filterType: 'custom',
            customFilterListOptions: {
              render: v => {
                if (v[0] && v[1] && this.state.ageFilterChecked) {
                  return [`Min Area: ${v[0]}`, `Max Area: ${v[1]}`];
                } else if (v[0] && v[1] && !this.state.ageFilterChecked) {
                  return `Min Area: ${v[0]}, Max Area: ${v[1]}`;
                } else if (v[0]) {
                  return `Min Area: ${v[0]}`;
                } else if (v[1]) {
                  return `Max Area: ${v[1]}`;
                }
                return [];
              },
              update: (filterList, filterPos, index) => {
                console.log('customFilterListOnDelete: ', filterList, filterPos, index);
  
                if (filterPos === 0) {
                  filterList[index].splice(filterPos, 1, '');
                } else if (filterPos === 1) {
                  filterList[index].splice(filterPos, 1);
                } else if (filterPos === -1) {
                  filterList[index] = [];
                }
  
                return filterList;
              },
            },
            filterOptions: {
              names: [],
              logic(area, filters) {
                if (filters[0] && filters[1]) {
                  return area < filters[0] || area > filters[1];
                } else if (filters[0]) {
                  return area < filters[0];
                } else if (filters[1]) {
                  return area > filters[1];
                }
                return false;
              },
              display: (filterList, onChange, index, column) => (
                <div>
                  <FormLabel>Area</FormLabel>
                  <FormGroup row>
                    <TextField
                      label='min'
                      value={filterList[index][0] || ''}
                      onChange={event => {
                        filterList[index][0] = event.target.value;
                        onChange(filterList[index], index, column);
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                    />
                    <TextField
                      label='max'
                      value={filterList[index][1] || ''}
                      onChange={event => {
                        filterList[index][1] = event.target.value;
                        onChange(filterList[index], index, column);
                      }}
                      style={{ width: '45%' }}
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.ageFilterChecked}
                          onChange={event => this.setState({ ageFilterChecked: event.target.checked })}
                        />
                      }
                      label='Separate Values'
                      style={{ marginLeft: '0px' }}
                    />
                  </FormGroup>
                </div>
              ),
            },
          }
        }, 
        {
          name: "population", 
          label:"Population",
          options:{
            filterType: 'custom',
            customFilterListOptions: {
              render: v => {
                if (v[0] && v[1] && this.state.ageFilterChecked) {
                  return [`Min Population: ${v[0]}`, `Max Population: ${v[1]}`];
                } else if (v[0] && v[1] && !this.state.ageFilterChecked) {
                  return `Min Population: ${v[0]}, Max Population: ${v[1]}`;
                } else if (v[0]) {
                  return `Min Population: ${v[0]}`;
                } else if (v[1]) {
                  return `Max Population: ${v[1]}`;
                }
                return [];
              },
              update: (filterList, filterPos, index) => {
                console.log('customFilterListOnDelete: ', filterList, filterPos, index);
  
                if (filterPos === 0) {
                  filterList[index].splice(filterPos, 1, '');
                } else if (filterPos === 1) {
                  filterList[index].splice(filterPos, 1);
                } else if (filterPos === -1) {
                  filterList[index] = [];
                }
  
                return filterList;
              },
            },
            filterOptions: {
              names: [],
              logic(population, filters) {
                if (filters[0] && filters[1]) {
                  return population < filters[0] || population > filters[1];
                } else if (filters[0]) {
                  return population < filters[0];
                } else if (filters[1]) {
                  return population > filters[1];
                }
                return false;
              },
              display: (filterList, onChange, index, column) => (
                <div>
                  <FormLabel>Population</FormLabel>
                  <FormGroup row>
                    <TextField
                      label='min'
                      value={filterList[index][0] || ''}
                      onChange={event => {
                        filterList[index][0] = event.target.value;
                        onChange(filterList[index], index, column);
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                    />
                    <TextField
                      label='max'
                      value={filterList[index][1] || ''}
                      onChange={event => {
                        filterList[index][1] = event.target.value;
                        onChange(filterList[index], index, column);
                      }}
                      style={{ width: '45%' }}
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.ageFilterChecked}
                          onChange={event => this.setState({ ageFilterChecked: event.target.checked })}
                        />
                      }
                      label='Separate Values'
                      style={{ marginLeft: '0px' }}
                    />
                  </FormGroup>
                </div>
              ),
            },
          }
        },
      ],
      objects: [],
      display_objects: [],    // objects on display right now
      searchText: "",         // text to search
      options: {
        onRowClick: (countryData) => window.location.assign("/countries/" + countryData[0]),
        selectableRowsHideCheckboxes: true,
        print: false,
        download: false,
        jumpToPage: true,
        search: false,
        searchPlaceholder: "Search Countries"
      },
    }
    this.customRender = this.customRender.bind(this);
    this.updateSearchQuery = this.updateSearchQuery.bind(this);
    this.searchBar = this.searchBar.bind(this);
  }
  
  componentDidMount() {
    this.fetchCountryData()
  };

  fetchCountryData = async () => {
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/countries?results_per_page=5000`,
    ); // data stores num_results, objects, page, total_pages
    const { objects } = data; //objects stores area, calling codes, etc.
    await this.setState({ objects: objects, display_objects: objects});
  };

  customRender = (value, tableMeta, updateValue) => {
    return <Highlighter
      highlightStyle={{backgroundColor: 'yellow'}}
      highlightClassName="highlight-class"
      searchWords={this.state.searchText.split(" ")}
      textToHighlight={value + ""}
    ></Highlighter>
  };

  async updateSearchQuery(searchQuery) {
    var list = searchQuery.toLowerCase().split(" "); // list of search queries
    // we need to first apply existing filters, if any
    var filtered = this.state.objects;
    if (searchQuery !== "") {
      filtered = this.state.objects.filter(o => {
        // check name, type, status, has_training, has_report, has_job
        // also check for filter in display
        var allTrue = false;
        for (var lower of list) {
          lower = lower.trim();
          const cur = o.name.toLowerCase().includes(lower) || 
            o.income.toLowerCase().includes(lower) || 
            o.status.toLowerCase().includes(lower);
          allTrue = allTrue || cur;
        }
        return allTrue
      })
    }

    // update the objects under display
    await this.setState({searchText: searchQuery, display_objects: filtered});
  }

  searchBar() {
    const BarStyling = {display: "flex", width: "800px"};
    return (<input
      style={BarStyling}
      className="searchBar"
      key="random1"
      value={this.state.searchText}
      placeholder={"Search Countries"}
      onChange={(e) => this.updateSearchQuery(e.target.value)}
    />)
  };

  render() {
    
    return (
      <div>
        <header className="country-header my-5">
          <h1 class = "model-page-title">Countries </h1>
          <br></br>
          <h1><this.searchBar></this.searchBar> </h1>
        </header> 
        <MUIDataTable
          columns={this.state.header}
          data={this.state.display_objects}
          options = {this.state.options}
        />
      </div>
    )
  }
}


export default Countries;
