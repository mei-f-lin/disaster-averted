import React from "react"
import Logo from "./logo2.ico"
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import NavItem from "react-bootstrap/esm/NavItem"

class Navigation extends React.Component{
  constructor(props) {
    super(props);
    // create a ref to store the textInput DOM element
    this.textInput = React.createRef();
  }

  search() {
    window.location.href = "/search/q=" + this.textInput.current.value;
  }

  render(){
    return (
      <Navbar style={{backgroundColor:"rgb(63, 195, 224)"}} expand="lg"  sticky="top">
        <Navbar.Brand style = {{fontWeight:"bold"}} href="/" className="nav-link">
          <img
            alt="logo"
            src={Logo}
            width="50"
            height="50"
            className="d-inline-block"
          />{' '}
          Disaster Averted
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link style = {{color:"black"}} href="/about">About</Nav.Link>
            <Nav.Link style = {{color:"black"}} eventKey="disabled" disabled>|</Nav.Link>
            <Nav.Link style = {{color:"black"}} href="/disasters">Disasters</Nav.Link>
            <Nav.Link style = {{color:"black"}} eventKey="disabled" disabled>|</Nav.Link>
            <Nav.Link style = {{color:"black"}} href="/Countries">Countries</Nav.Link>
            <Nav.Link style = {{color:"black"}} eventKey="disabled" disabled>|</Nav.Link>
            <Nav.Link style = {{color:"black"}} href="/organizations">Organizations</Nav.Link>
            <Nav.Link style = {{color:"black"}} eventKey="disabled" disabled>|</Nav.Link>
            <Nav.Link style = {{color:"black"}} href="/visualizations">Visualizations</Nav.Link>
            <Nav.Link style = {{color:"black"}} eventKey="disabled" disabled>|</Nav.Link>
            <Nav.Link style = {{color:"black"}} href="/providerVisualizations">Provider Visualizations</Nav.Link>
          </Nav>
          <Form className="justify-content-center" inline onSubmit={(e) => {
              e.preventDefault();
            }}>
            <FormControl 
              type="text" 
              placeholder="Search" 
              className="mr-sm-2" 
              ref={this.textInput}
              onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    this.search();
                  }
                }}/>
          <Button style={{fontWeight:"500"}} variant="outline-light" onClick={()=>this.search()}>Search</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}
export default Navigation;
