import React, { Component } from "react"; 
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import Highlighter from "react-highlight-words";
import './bars.css';
import './ModelPage.css';
import {
  FormGroup,
  FormLabel,
  TextField,
  Checkbox,
  FormControlLabel,
} from '@material-ui/core';

class Disasters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: [],              // input in searchText
      header: [
        {
          name: "id",
          label: "ID",
          options:{
            filter: false, 
            sort: false,
            display: false
          }
        },
        {
          name: "name",
          label: "Name",
          options: {
            filter: false,
            /* enable highlighting */
            customBodyRender: this.customRender
          },
        },
        {
          name: "type",
          label: "Type",
          options: {
            filterType: 'multiselect',
            customFilterListOptions: {
              render: v => {return "Type: " + v}
            },
            /* enable highlighting */
            customBodyRender: this.customRender
          }
        },
        {
          name: "year",
          label: "Year",
          options:{
            filterType: 'custom',
            customFilterListOptions: {
              render: v => {
                if (v[0] && v[1] && this.state.ageFilterChecked) {
                  return [`Min Year: ${v[0]}`, `Max Year: ${v[1]}`];
                } else if (v[0] && v[1] && !this.state.ageFilterChecked) {
                  return `Min Year: ${v[0]}, Max Year: ${v[1]}`;
                } else if (v[0]) {
                  return `Min Year: ${v[0]}`;
                } else if (v[1]) {
                  return `Max Year: ${v[1]}`;
                }
                return [];
              },
              update: (filterList, filterPos, index) => {
                console.log('customFilterListOnDelete: ', filterList, filterPos, index);
  
                if (filterPos === 0) {
                  filterList[index].splice(filterPos, 1, '');
                } else if (filterPos === 1) {
                  filterList[index].splice(filterPos, 1);
                } else if (filterPos === -1) {
                  filterList[index] = [];
                }
  
                return filterList;
              },
            },
            filterOptions: {
              names: [],
              logic(year, filters) {
                if (filters[0] && filters[1]) {
                  return year < filters[0] || year > filters[1];
                } else if (filters[0]) {
                  return year < filters[0];
                } else if (filters[1]) {
                  return year > filters[1];
                }
                return false;
              },
              display: (filterList, onChange, index, column) => (
                <div>
                  <FormLabel>Year</FormLabel>
                  <FormGroup row>
                    <TextField
                      label='min'
                      value={filterList[index][0] || ''}
                      onChange={event => {
                        filterList[index][0] = event.target.value;
                        onChange(filterList[index], index, column);
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                    />
                    <TextField
                      label='max'
                      value={filterList[index][1] || ''}
                      onChange={event => {
                        filterList[index][1] = event.target.value;
                        onChange(filterList[index], index, column);
                      }}
                      style={{ width: '45%' }}
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={this.state.ageFilterChecked}
                          onChange={event => this.setState({ ageFilterChecked: event.target.checked })}
                        />
                      }
                      label='Separate Values'
                      style={{ marginLeft: '0px' }}
                    />
                  </FormGroup>
                </div>
              ),
            },
          }
        },
        {
          name: "country",
          label: "Country",
          options: {
            filterType: 'multiselect',
            customFilterListOptions: {
              render: v => {return "Country: " + v}
            },
            /* enable highlighting */
            customBodyRender: this.customRender
          }
        },
        {
          name: "status",
          label: "Status",
          options: {
            filterType: 'multiselect',
            customFilterListOptions: {
              render: v => {return "Status: " + v}
            },
            /* enable highlighting */
            customBodyRender: this.customRender
          }
        },
      ],
      options: {
        onRowClick: (disasterData) => window.location.assign("/disasters/" + disasterData[0]),
        selectableRowsHideCheckboxes: true,
        print: false,
        download: false,
        rowsPerPageOptions: [10, 20, 50, 100],
        jumpToPage: true,
        search: false,
        caseSensitive: false,
        searchOpen: false,
        searchPlaceholder: "Search Disasters",
      },
      num_results: 0,
      objects: [],                // collection of all objects
      display_objects: [],        // objects actually in display
      total_pages:0,
      country_ids: [],
      num: -3,
      links: []
    }
    this.customRender = this.customRender.bind(this);
    this.reRender = this.reRender.bind(this);
    this.updateSearchQuery = this.updateSearchQuery.bind(this);
    this.searchBar = this.searchBar.bind(this);
  }

  componentDidMount() {
    this.fetchDisasterData();
  }

  fetchDisasterData = async () => {
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/disasters?results_per_page=5000`,
    ); // data stores num_results, objects, page, total_pages
    const { objects } = data; //objects stores area, calling codes, etc.
    await this.setState({ objects: objects, display_objects: objects});
  }

  customRender = (value, tableMeta, updateValue) => {
    return <Highlighter
      highlightStyle={{backgroundColor: 'yellow'}}
      highlightClassName="highlight-class"
      searchWords={this.state.searchText}
      textToHighlight={value + ""}
    ></Highlighter>
  };

  async updateSearchQuery(searchQuery) {
    var list = searchQuery.toLowerCase().split(" "); // list of search queries
    // we need to first apply existing filters, if any
    var filtered = this.state.objects;
    if (searchQuery !== "") {
      filtered = this.state.objects.filter(o => {
        // check name, type, status, has_training, has_report, has_job
        // also check for filter in display
        var allTrue = false;
        for (var lower of list) {
          lower = lower.trim();
          const cur = o.name.toLowerCase().includes(lower) || 
            o.type.toLowerCase().includes(lower) || 
            o.status.toLowerCase().includes(lower) || 
            o.country.toLowerCase().includes(lower);
          allTrue = allTrue || cur;
        }
        return allTrue
      })
    }
    var words = searchQuery.split(" ");
    // update the objects under display
    await this.setState({searchText: words, display_objects: filtered});
  }

  searchBar() {
    const BarStyling = {display: "flex", width: "800px"};
    return (<input
      style={BarStyling}
      className="searchBar"
      key="random1"
      value={this.state.searchText.join(" ")}
      placeholder={"Search Disasters"}
      onChange={(e) => this.updateSearchQuery(e.target.value)}
    />)
  };

  reRender = () => {
    return <MUIDataTable
      columns={this.state.header}
      data={this.state.display_objects}
      options = {this.state.options}
    />
  };

  render() {
    return (
      <div className="DisasterModel"> 
        <header className="disasters-header my-5">
          <h1 class = "model-page-title">Disasters </h1> 
          <br></br>
          <h1 className> <this.searchBar></this.searchBar> </h1>
        </header>
        <this.reRender></this.reRender>
      </div>
    )
  }
}


export default Disasters;
