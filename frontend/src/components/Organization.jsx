import React, { Component } from "react";
import {Card, Container, CardDeck, Alert, Row, Col} from 'react-bootstrap';
import axios from 'axios';
import MapContainer from './MapsAPI';
import Typography from '@material-ui/core/Typography';

class Organization extends Component {
  constructor(props) {
    // since we are extending class Table so we have to use super in 
    // order to override Component class constructor
    super(props)
    this.state = {
      attributes: ["Type", "Jobs Available", "Reports Available", 
        "Training Available", "Website", "ID", "Jobs", "Reports", 
        "Short Name", "Status", "Trainings", "Country", "Disaster"
      ],
      has_job: "",
      has_report: "",
      has_training: "",
      homepage: "",
      id: 0,
      jobs: 0,
      logo_url: "",
      name: "",
      reports: 0,
      s_name: "",
      status: "",
      alerts: ['success', 'warning'],
      alertType: -1,
      trainings: 0,
      type: "",
      country_id: 0, 
      disaster_id: 0,
      country: "",
      disaster: "",

      //for disaster card
      disaster_attributes: ["Name", "Type", "Day", "Month", "Year"],
      months: ["January", "February", "March", "April", "May", "June", "July", 
        "August", "September", "October", "November", "December"
      ],
      images: [
        "https://media3.s-nbcnews.com/j/newscms/2020_35/3407271/200825-california-lnu-fire-jm-0957_c9b8238c91004412d2f816fdff345a93.fit-1240w.jpg", 
        "https://www.redzone.co/wp-content/uploads/2018/04/river-flood-1030x687.jpg", 
        "https://www.redrisk.com/redeng/Projects/projects/project12/topimage.jpg", 
        "https://iadsb.tmgrup.com.tr/ed03ed/0/0/0/0/800/518?u=https://idsb.tmgrup.com.tr/2019/01/31/arctic-cold-wave-sends-temps-to-record-lows-in-us-midwest-expected-to-ease-1548943614299.jpg", 
        "https://media.tzuchi.us/wp-content/uploads/2018/11/09152516/ecuadorearthquake2016-4.jpg", 
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBhnM-GByo4Gvz7MurvqxD1Z2vIf7EZ6VezA&usqp=CAU"
      ],
      type_id: "",
      num: -1,
      day: 0,
      month: 0,
      year: 0,

      //for country card
      country_attributes: ["Capital", "Subregion", "Population", "Income Level"],
      flag: "",
      population: "",
      income: "",
      capital: "",
      subregion: "",
    }
  }
  componentDidMount() {
    this.fetchCountryData()
  }

  fetchCountryData = async () => {
    var link = window.location.href
    var pieces = link.split("/")
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/orgs/` + pieces[pieces.length - 1],
    ); 
    const { has_job, has_report, has_training, homepage, id, jobs, logo_url, 
      reports, s_name, status, trainings, type } = data;
    var { name } = data
    this.setState({ has_job, has_report, has_training, homepage, id, jobs, 
      logo_url, name, reports, s_name, status, trainings, type });
    if(this.state.status === "active") {
      this.state.alertType = 0;
    } else {
      this.state.alertType = 1;
    }
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/orgs_link/` + this.state.id,
    );
    const { country_id, disaster_id } = data;
    this.setState({ country_id, disaster_id });
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/disasters/` + this.state.disaster_id,
    );
    var { name, type_id, day, month, year } = data;
    this.setState({ disaster: name,type_id, day, month, year });
    var num2 = 0;
    if (type_id === "WF") {
      num2 = 0;
    }
    else if (type_id === "FL"){
      num2 = 1;
    }
    else if(type_id === "FF"){
      num2 = 1;
    }
    else if(type_id === "AC"){
      num2 = 2;
    }
    else if(type_id === "CW"){
      num2 = 3;
    }
    else if(type_id === "EQ"){
      num2 = 4;
    }
    else{
      num2 = 5;
    }
    this.state.num = num2;
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/countries/` + this.state.country_id,
    );
    var { name, flag, population, income, capital, subregion } = data;
    this.setState({ country: name, flag, population, income, capital, subregion });
  }

  render() {
    return (
      <div className="Organization">
        <header className="Organization-header my-5">
          <h1>{this.state.name}</h1>
          <h4>{this.state.attributes[4]}: <a href= {this.state.homepage}>{this.state.homepage}</a></h4>
          <MapContainer></MapContainer>
          <img 
            alt = ""
            src = {this.state.logo_url} 
            height = {228}
            style={{
              position: 'relative',
              left: '-20%',
              right: '0%',
            }}
          />
        </header>
        <body class="App">
            <center>
              <Alert variant={this.state.alerts[this.state.alertType]}>
                <Alert.Heading>{this.state.attributes[9]}</Alert.Heading>
                <p className="mb-0">
                  {this.state.status}
                </p>
              </Alert>
              <div>
              <Container>
                <Row className='my-5'>
                    <Col>
                      <h5><b>{this.state.attributes[0]}</b></h5>
                      <h5>{this.state.type}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[1]}</b></h5>
                      <h5>{this.state.has_job}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[3]}</b></h5>
                      <h5>{this.state.has_training}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[2]}</b></h5>
                      <h5>{this.state.has_report}</h5>
                    </Col>
                </Row>
                <Row className='my-5'>
                    <Col>
                      <h5><b>{this.state.attributes[8]}</b></h5>
                      <h5>{this.state.s_name}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[6]}</b></h5>
                      <h5>{this.state.jobs}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[10]}</b></h5>
                      <h5>{this.state.trainings}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[7]}</b></h5>
                      <h5>{this.state.reports}</h5>
                    </Col>  
                </Row>
              </Container>
              </div>
              
              <CardDeck style={{ width: '80%' }}>
                <Card>
                <a style={{ cursor: 'pointer', color: 'black'}} href ={"/disasters/" + this.state.disaster_id}>
                  <Card.Img variant="top" src = {this.state.images[this.state.num]} height={260} />
                  <Card.Body>
                    <Card.Title>
                      <b>{this.state.attributes[12]}: {this.state.disaster}</b>
                    </Card.Title>
                    <Typography>
                      {this.state.disaster_attributes[2]}: {this.state.day}
                    </Typography>
                    <Typography>
                      {this.state.disaster_attributes[3]}: {this.state.months[this.state.month - 1]}
                    </Typography>
                    <Typography>
                      {this.state.disaster_attributes[4]}: {this.state.year}
                    </Typography>
                  </Card.Body>
                  </a>
                </Card>
                <Card>
                  <a style={{ cursor: 'pointer', color: 'black'}} href ={"/countries/" + this.state.country_id}>
                  <Card.Img variant="top" src={this.state.flag} height={260} />
                  <Card.Body>
                    <Card.Title>
                    <b>{this.state.attributes[11]}: {this.state.country}</b>
                    </Card.Title>
                    <Typography>
                      {this.state.country_attributes[0]}: {this.state.capital}
                    </Typography>
                    <Typography>
                      {this.state.country_attributes[1]}: {this.state.subregion}
                    </Typography>
                    <Typography>
                      {this.state.country_attributes[2]}: {this.state.population}
                    </Typography>
                    <Typography>
                      {this.state.country_attributes[3]}: {this.state.income}
                    </Typography>
                  </Card.Body>
                  </a>
                </Card>
              </CardDeck>
              <br></br>
              <p></p>
            </center>
          </body>
      </div>
    );
  }
}

export default Organization;