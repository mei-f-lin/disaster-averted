import React, { Component } from "react";
import { 
  BarChart, CartesianGrid, Legend, XAxis, YAxis, Tooltip, Bar,
  ScatterChart, Scatter, Label, PieChart, Pie, ResponsiveContainer, Cell
} from 'recharts';
import axios from 'axios';
import "./plots.css"
import { renderActiveShape } from './activePie.jsx';
import { ProviderCountryTooltip } from './customToolTip.jsx';

class ProviderVisualizations extends Component {
    constructor(props) {
      super(props)
      this.state = {
        species:   [],      // array of disaster objects
        countries: [],      // array of country objects
        habitats:  [],      // array of organization objects
        activeIndex: 0,     // which pie slice are we hovering on?
        regionColors: {
          'Africa': "#1B9E77", 
          'Europe': "#D95F02", 
          'Americas': "#7570B3",
          'Asia': "#E7298A",
          'Oceania': "#66A61E",
          'Polar': "#E6AB02"
        }
      }
      this.numberWithCommas = this.numberWithCommas.bind(this);
      this.countLevels = this.countLevels.bind(this);
      this.onPieEnter = this.onPieEnter.bind(this);
    }
  
    componentDidMount() {
      this.getData();
    }

    // counts the number of items at each level of a categorical attribute
    countLevels(data, attribute) {
      // we have to count how many items are in each level of attribute, 
      var types = {}
      data.forEach(e => {
        // console.log(disaster)
        if (!(e[attribute] in types)) {
          types[e[attribute]] = 0
        }
        types[e[attribute]] += 1
      });

      // go through the types dictionary and make the data ready for plots 
      var ret = []
      for(var key in types) {
        ret.push({'name': key, 'value': types[key]})
      }
      // sort bars in desending order
      ret.sort((lhs, rhs) => - (lhs.value - rhs.value));

      return ret;
    }

    onPieEnter = (_, index) => {
      this.setState({
        activeIndex: index,
      });
    };
  
    async getData() {
      var { data } = await axios.get(
        `https://critterycovery.me/api/species`,
      ); // data stores num_results, objects, page, total_pages
      var { species } = data;
      this.setState({ species: this.countLevels(species, 'phylum') });

      var { data } = await axios.get(
        `https://critterycovery.me/api/countries`,
      ); // data stores num_results, objects, page, total_pages
      var { countries } = data;

      var filtered = countries.filter(function(value, index){ 
        return value['total_pop'] > 0 & value['area'] > 0;
      });
      this.setState({ countries: filtered});

      var { data } = await axios.get(
        `https://critterycovery.me/api/habitats`,
      ); // data stores num_results, objects, page, total_pages
      var { habitats } = data;
      this.setState({ habitats: habitats });

      // TODO: Add more information to tool tip (need custom function)
      const isMarine = this.countLevels(habitats, 'marine');
      // don't like true or false naming scheme, change schemes
      var renamed = [];
      isMarine.forEach(e => {
        if (e['name'] == 'true') { 
          renamed.push({'name': "Marine Habitats", 'value': e['value']}) 
        } else {
          renamed.push({'name': "Non-Marine Habitats", 'value': e['value']}) 
        }
      });
      this.setState({ habitats: renamed });
    }

    numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    render() {
      return (
        <container ProviderVisualizations>
          <h1>Species' Phylum Categorization</h1>
          <ResponsiveContainer className="scatter" height={1080*0.5} width='90%'>
            <BarChart width={this.props.width} height={this.props.height} 
              data={this.state.species} layout='vertical'
              margin={{ top: 5, right: 100, bottom: 25, left: 50 }}
            >
              <CartesianGrid 
                strokeDasharray="3 3"
                stroke="#555555" 
                horizontal={false}
              />
              <YAxis type='category' dataKey='name' 
                interval={0} width={200} 
              />
              <XAxis type='number' padding={{'left': 0}}>
                <Label value="Number of Species" 
                  offset={-20} position="insideBottom" 
                  fontSize={20}
                />
              </XAxis>
              <Tooltip 
              formatter={(value, common_name, props) => ( [value, "Total Species in Phylum"] )}
              />\
              <Bar layout="horizontal" dataKey="value" fill="#8884d8" />
            </BarChart>
          </ResponsiveContainer>

          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <h1>Countries</h1>
          <ResponsiveContainer className="scatter" height={1200*0.5 - 20} width='90%'>
            <ScatterChart width={this.props.width} height = {this.props.height}
              margin={{ top: 20, right: 100, bottom: 20*2.5, left: 200 } }
            >
              <CartesianGrid strokeDasharray="3 3" stroke="#555555" />
              <XAxis 
                dataKey="area" name="Area" type="number" 
                scale="log" domain={[1, 'auto']} 
                ticks={
                  Array.apply(0, Array(9)).map(function(_,b) { return Math.pow(10, b); })
                } 
                tickFormatter={this.numberWithCommas}
              >
                <Label value="Area (km squared)" offset={-20} position="insideBottom" fontSize={20} />
              </XAxis>
              <YAxis dataKey="total_pop" name="Population" type="number" 
                scale="log" domain={[1, 'auto']}
                ticks={
                  Array.apply(0, Array(11)).map(function(_,b) { return Math.pow(10, b); })
                } 
                tickFormatter={this.numberWithCommas}
              >
                <Label value="Population" offset={+80} position="left" fontSize={20} angle={-90} 
                  style={{ textAnchor: 'middle'}}
                />
              </YAxis>
              <Tooltip 
                cursor={{ stroke: 'red', strokeWidth: 2 }}
                content={<  ProviderCountryTooltip />}
              />
              <Scatter data={this.state.countries}>
                {
                  this.state.countries.map((c, index) => {
                    return <Cell key={`cell-${index}`} fill={this.state.regionColors[c.region]} />
                  })
                } 
              </Scatter>
              <Legend 
                layout="horizontal" verticalAlign="top" align="center" 
                payload = {
                  Object.keys(this.state.regionColors).map((k, index) => {
                    return {
                      'value': k, 'type': 'circle',
                      'id': `ID${index}`, 'color': this.state.regionColors[k]
                    }
                  })
                }
                height={36}
              >
              </Legend>
            </ScatterChart>
          </ResponsiveContainer>

          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <h1>Habitats</h1>
          <ResponsiveContainer className="pie" height={550} width='100%'>
            <PieChart width={this.props.width} height={this.props.height}
              margin={{ top: 20*2.5, bottom: 20*2.5, left: this.props.width }}>
              <Pie data={this.state.habitats} 
                activeIndex={this.state.activeIndex}
                cx={this.props.width / 2}
                cy={200}
                activeShape={(props) => renderActiveShape(props, 457)}
                data={this.state.habitats}
                cx="50%"
                cy="50%"
                innerRadius={200}
                outerRadius={240}
                dataKey="value"
                onMouseEnter={this.onPieEnter}
              > {
                  this.state.habitats.map((entry, index) => {
                    const colors = ["#eb9234", "#8884d8"];
                    return <Cell key={`cell-${index}`}
                                fill={colors[index]}
                                stroke={undefined}
                            />
                  })
              } </Pie>
            </PieChart>
          </ResponsiveContainer>

        </container>
      )
    }

}

export default ProviderVisualizations;
