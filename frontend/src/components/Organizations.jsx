import React, { Component } from "react";
import './../App.css';
import {Card, CardColumns, Container} from 'react-bootstrap';
import axios from 'axios';
import { Pagination } from "@material-ui/lab";
import Select from 'react-select';
import { statusOptions, typeOptions, jobTrainingReportOptions, countriesOptions } from './filterSetup.js';
import './bars.css';
import Highlighter from "react-highlight-words";
import Typography from '@material-ui/core/Typography';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import './ModelPage.css';

class Organizations extends Component {
  constructor(props) {
    // since we are extending class Table so we have to use super in order to override Component class constructor
    super(props)
    this.state = {
      header: ["Name", "Type", "Headquarters", "Status", 
               "Job Openings", "Training Opportunities", "Wrote Reports"],
      num_results: 0,               // total number of objects displayed
      objects: [],                  // all of the organization instances
      display_objects: [],          // what's displayed on each page (~10)
      display_objects_all: [],      // what's displayed across all pages
      applied_filters: {},          // all currently applied filters of form
                                    // attribute: {accepted values}
      current_page: 1,              // currente page number
      cards_per_page: 9,            // total cards to display in a page
      total_pages:0,                // total number of pages displayed
      links: [],                    // links to country
      searchQuery: '',              // current input in search bar
      countries: [],                // stores the list of countries
      sorting_criteria: 'Sort By: ',  // stores the current sorting criteria
      sorting_options: [            // all values of which we can sort on
        {type: "ascending", attribute: "name", label: "Name Asc.", id: 1 },
        {type: "descending", attribute: "name", label: "Name Desc.", id: 2 },
        {type: "ascending", attribute: "type", label: "Type Asc.", id: 3 },
        {type: "descending", attribute: "type", label: "Type Desc.", id: 4 },
        {type: "ascending", attribute: "country_name", label: "HQ Asc.", id: 5 },
        {type: "descending", attribute: "country_name", label: "HQ Desc.", id: 6 },
      ],
      debug: -1                     // debugging purposes
    }

    // need to bind this to function org item by default
    this.orgCard = this.orgCard.bind(this);
    this.filterBar = this.filterBar.bind(this);
    this.searchBar = this.searchBar.bind(this);
    this.sortBar = this.sortBar.bind(this);
    this.checkObject = this.checkObject.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.updateSearchQuery = this.updateSearchQuery.bind(this);
  }

  componentDidMount() {
    this.fetchOrgData().then(
      this.fetchCountryLinkData
    ).then(
      this.fetchCountryData
    );
    //this.fetchCountryData();
    // this.fetchCountryLinkData();
  }

  fetchOrgData = async () => {
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/orgs?results_per_page=5000`,
    ); // data stores num_results, objects, page, total_pages
    var { num_results, total_pages } = data; //objects stores area, calling codes, etc.
    var { objects } = data;
    total_pages = Math.ceil(num_results / this.state.cards_per_page);
    await this.setState({ num_results, objects, total_pages});
  }

  fetchCountryLinkData = async() => {
    // grab disaster links
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/orgs_link?results_per_page=5000`,
    ); // data stores num_results, objects, page, total_pages
    var { objects } = data;
    await this.setState({links: objects});
  }

  fetchCountryData = async() => {
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/countries?results_per_page=5000`,
    ); // data stores num_results, objects, page, total_pages
    const { objects } = data; //objects stores area, calling codes, etc.
    await this.setState({ countries: objects });

    // we first sort all organizations and their links by the id
    this.state.objects.sort((lhs, rhs) => { return (lhs.id - rhs.id) });
    this.state.links.sort((lhs, rhs) => { return (lhs.id - rhs.id) });

    // extract the country name's of the organization's headquarter's
    for (var idx in this.state.links) {
      const country_id = this.state.links[idx]['country_id'];
      const country_instance = this.state.countries.find(
        c => c['id'] === country_id
      );
      this.state.objects[idx]['country_name'] = country_instance['name'];
    };

    var display_objects = this.state.objects.slice(0, this.state.cards_per_page);
    await this.setState(
      { display_objects: display_objects, 
        display_objects_all: this.state.objects
      });
    await this.handleSortChange(1); // default sort by name ascending
  }

  /**
   * A grid item for an organization.
   */
  orgCard() {
    if (this.state.display_objects.length === 0) {
      return (
        <h1>Sorry, no matching organizations found! </h1>
      )
    }
    return (
      <Container fluid className = "fullclick">
      <CardColumns>
        {this.state.display_objects.map((organization) => {
          return(
            <a style={{ cursor: 'pointer', color: 'black'}} href ={"/organizations/" + organization["id"]}>
              <Card style={{ width: '95%', height:"auto"}}>
                <Card.Img variant="top" src={organization["logo_url"]} className="cardImage" />
                <Card.Body>
                  <Card.Title>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      // searchWords={this.state.searchQuery}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={organization["name"]}>
                    </Highlighter>
                  </Card.Title>
                  <Typography>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={this.state.header[1] + `: ` + organization["type"]}>
                    </Highlighter>
                  </Typography>
                  <Typography>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={this.state.header[2] + `: ` + organization["country_name"]}>
                    </Highlighter>
                  </Typography>
                  <Typography>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={this.state.header[3] + `: ` + organization["status"]}>
                    </Highlighter>
                  </Typography>
                  <Typography>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={this.state.header[4] + `: ` + organization["has_job"]}>
                    </Highlighter>
                  </Typography> 
                  <Typography>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={this.state.header[5] + `: ` + organization["has_training"]}>
                    </Highlighter>
                  </Typography>
                  <Typography>
                    <Highlighter
                      highlightStyle={{backgroundColor: 'yellow'}}
                      searchWords={this.state.searchQuery.split(" ")}
                      autoEscape={true}
                      textToHighlight={this.state.header[6] + `: ` + organization["has_report"]}>
                    </Highlighter>
                  </Typography>
                </Card.Body>
              </Card>
            </a>
          )
        })}
      </CardColumns>
    </Container>
    )
  }

  // update current "data frame"
  handleChangePage(pageNumber) {
    var display_objects = this.state.display_objects_all.slice(
        (pageNumber-1) * this.state.cards_per_page, 
        pageNumber * this.state.cards_per_page
    );
    var total_pages = Math.ceil(
      this.state.display_objects_all.length / this.state.cards_per_page
    );
    this.setState({display_objects});
    this.setState({total_pages: total_pages});
    this.setState({current_page: pageNumber});
    this.setState({num_results: this.state.display_objects_all.length});
  }

  // using this.state.applied_filters to check if an object should
  // be displayed
  checkObject(o) {
    for (var attribute in this.state.applied_filters) {
      const acceptedValues = this.state.applied_filters[attribute].map(
        obj => obj['value']
      );
      if (!acceptedValues.includes(o[attribute])) { return false; }
    }
    return true;
  }

  async handleFilterChange(attribute, selectedOptions) {
    const arrayLength = selectedOptions.length;
    const bool = this.state.applied_filters.hasOwnProperty(attribute);

    if (bool) {
      // this attribute exists in current array of filters
      if (arrayLength === 0) {
        // delete this attribute filter, since nothing is selected
        delete this.state.applied_filters[attribute]
      }
      else {
        // this attribute exists so we have selected Options, so we update
        this.state.applied_filters[attribute] = selectedOptions
      }
    }
    else {
      // attribute filter does not exist in current array of filters
      if (arrayLength !== 0) {
        // add the selected option for this attribute as filter
        this.state.applied_filters[attribute] = selectedOptions
      }
      // in case where no option is selected and filters for this attribute
      // does not already exist, nothing to do
      else { return; }
    }

    // 2. we know know the options has been correctly updated, so that 
    // we filter down the objects
    var updated_display_objects = this.state.objects.filter(o => this.checkObject(o));
    await this.setState({display_objects_all: updated_display_objects});

    // 3. render the filtered down objects
    await this.handleChangePage(this.state.current_page);
  }

  async updateSearchQuery(searchQuery) {
    var list = searchQuery.toLowerCase().split(" "); // list of search queries
    // we need to first apply existing filters, if any
    var filtered = this.state.objects.filter(o => this.checkObject(o));
    if (searchQuery !== "") {
      filtered = this.state.objects.filter(o => {
        // check name, type, status, has_training, has_report, has_job
        // also check for filter in display
        var allTrue = false;
        for (var lower of list) {
          lower = lower.trim();
          const c_id = this.state.links.find(c => c['id'] === o.id)['country_id'];
          const c_name = this.state.countries.find(c => c['id'] === c_id)['name'];
          const cur = o.name.toLowerCase().includes(lower) || 
            o.type.toLowerCase().includes(lower) || 
            o.status.toLowerCase().includes(lower) || 
            o.has_training.toLowerCase().includes(lower) || 
            o.has_report.toLowerCase().includes(lower) || 
            o.has_job.toLowerCase().includes(lower) || 
            c_name.toLowerCase().includes(lower) || 
            this.state.header.join("").toLowerCase().includes(lower);
          allTrue = allTrue || cur;
        }
        return allTrue
      })
    }

    // update the objects under display
    await this.setState({searchQuery: searchQuery, display_objects_all: filtered});
    // repopulate the page display
    await this.handleChangePage(this.state.current_page);
  }

  async handleSortChange(sort_criteria_id) {
    // find the sorting option with this id and set the label appropriately
    const criteria = this.state.sorting_options.find(
      c => c.id === parseInt(sort_criteria_id)
    );
    await this.setState({sorting_criteria: `Sort By: `+ criteria['label']});
    
    const attribute = criteria.attribute;
    const type = criteria.type;
    // now, sort this.state.display_objects_all by the selected criteria
    function sortAscending(lhs, rhs) {
      var nameA = lhs[attribute].toLowerCase().replace(/[^\x00-\x7F]/g, ""); // ignore upper and lowercase
      var nameB = rhs[attribute].toLowerCase().replace(/[^\x00-\x7F]/g, ""); // ignore upper and lowercase
      if (nameA < nameB) { return -1; }
      else if (nameA > nameB) { return 1; }
      else { return 0; }
    };

    if (type === "ascending") {
      this.state.display_objects_all.sort(sortAscending);
    }
    else if (type === "descending") {
      this.state.display_objects_all.sort(
        function(lhs, rhs) { return (-sortAscending(lhs, rhs)) }
      );
    }
    
    // repopulate the page display
    await this.handleChangePage(this.state.current_page);
  }

  filterBar({options, attribute, placeholder, cssStyle}) {
    return <Select
      isMulti
      name="filter_bar"
      isSearchable={false}
      showArrow={true}
      options={options}
      className={cssStyle}
      placeholder={placeholder + " filter"}
      noOptionsMessage="No data matches this filter!"
      onChange={(selectedOption) => this.handleFilterChange(attribute, selectedOption)}
      classNamePrefix="select"
    />
  }

  searchBar() {
    const BarStyling = { width: "20rem", background: "#FFFFFF", padding: "0.5rem" };
    return <input
      style={BarStyling}
      key="random1"
      value={this.state.searchQuery}
      placeholder={"Search Organizations"}
      onChange={(e) => this.updateSearchQuery(e.target.value)}
    />
  }

  sortBar() {
    return <DropdownButton
      alignRight
      title={this.state.sorting_criteria}
      id="dropdown-menu-align-right"
      className="sortBar"
      onSelect={(e) => this.handleSortChange(e) }
    >
      {
        this.state.sorting_options.map((option) => {
          return (<Dropdown.Item eventKey={option.id}>
            {option['label']}
          </Dropdown.Item>)
        })
      }
    </DropdownButton>
  }

  render() {
    return (
      <div className="organizations">
        <header className="organizations-header my-5">
          <h1 class = "model-page-title">Organizations</h1>
        </header>

        <div>
          <this.sortBar></this.sortBar>
          <this.searchBar></this.searchBar>
        </div>

        <div>
          <this.filterBar options={statusOptions} 
            attribute="status" placeholder = "Status" cssStyle="statusBar">
          </this.filterBar>

          <this.filterBar options={typeOptions} 
            attribute="type" placeholder = "Type" cssStyle="typeBar">
          </this.filterBar>

          <this.filterBar options={countriesOptions} 
            attribute="country_name" placeholder = "HQ" cssStyle="countryBar">
          </this.filterBar>
        </div>

        <this.filterBar options={jobTrainingReportOptions} 
          attribute="has_job" placeholder = "Job" cssStyle="yesNoBar">
        </this.filterBar>

        <this.filterBar options={jobTrainingReportOptions} 
          attribute="has_training" placeholder = "Training" cssStyle="yesNoBar">
        </this.filterBar>

        <this.filterBar options={jobTrainingReportOptions} 
          attribute="has_report" placeholder = "Report" cssStyle="yesNoBar">
        </this.filterBar>

        <this.orgCard></this.orgCard>

        <div className="page_bar" style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}>
          <Pagination
              variant="outlined"
              count={this.state.total_pages}
              onChange={(_, page) => this.handleChangePage(page)}
              showFirstButton
              showLastButton
              size="large"
          />
        </div>
        <div>Total Organizations: {this.state.num_results}</div>
      </div>
    )
  }
}

export default Organizations;
