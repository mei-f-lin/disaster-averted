import React, { Component } from "react";
import { 
  BarChart, CartesianGrid, Legend, XAxis, YAxis, Tooltip, Bar,
  ScatterChart, Scatter, Label, PieChart, Pie, ResponsiveContainer, Cell
} from 'recharts';
import axios from 'axios';
import "./plots.css"
import { renderActiveShape } from './activePie.jsx';
import { CountryTooltip }  from './customToolTip.jsx';

class Visualizations extends Component {
    constructor(props) {
      super(props)
      this.state = {
        disasters: [],          // array of disaster objects
        countries: [],          // array of country objects
        orgs:      [],          // array of organization objects
        activeIndex: 0,         // which pie slice are we hovering on?
        incomeColors: {
          'Low income': "#E31A1C", 
          'Lower middle income': "#FD8D3C", 
          'Upper middle income': "#FECC5C",
          'High income': "#FFFFB2"
        }
      }
      this.numberWithCommas = this.numberWithCommas.bind(this);
      this.countLevels = this.countLevels.bind(this);
      this.onPieEnter = this.onPieEnter.bind(this);
    }
  
    componentDidMount() {
      this.getData();
    }

    // counts the number of items at each level of a categorical attribute
    countLevels(data, attribute) {
      // we have to count how many items are in each level of attribute, 
      var types = {}
      data.forEach(e => {
        // console.log(disaster)
        if (!(e[attribute] in types)) {
          types[e[attribute]] = 0
        }
        types[e[attribute]] += 1
      });

      // go through the types dictionary and make the data ready for plots 
      var ret = []
      for(var key in types) {
        ret.push({'name': key, 'value': types[key]})
      }
      // sort bars in desending order
      ret.sort((lhs, rhs) => - (lhs.value - rhs.value));

      return ret;
    }

    onPieEnter = (_, index) => {
      this.setState({
        activeIndex: index,
      });
    };
  
    async getData() {
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/disasters?results_per_page=5000`,
      ); // data stores num_results, objects, page, total_pages
      var { objects } = data; //objects stores area, calling codes, etc.

      // TODO: Add more information to tool tip (need custom function)
      const disasters = this.countLevels(objects, 'type');
      this.setState({ disasters });

      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/countries?results_per_page=5000`,
      ); // data stores num_results, objects, page, total_pages
      var { objects } = data; //objects stores area, calling codes, etc.

      // TODO: Make Legend Interactive (ie. highlight all income levels)
      this.setState({ countries: objects});

      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/orgs?results_per_page=5000`,
      ); // data stores num_results, objects, page, total_pages
      var { objects } = data;
      this.setState({ orgs: objects });

      // TODO: Add more information to tool tip (need custom function)
      const orgs = this.countLevels(objects, 'type');
      this.setState({ orgs });
    }

    numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    render() {
      return (
        <container Visualizations>
          <br></br>
          <h1>Disasters</h1>
          <ResponsiveContainer className="scatter" height={1080*0.5} width='90%'>
            <BarChart width={this.props.width} height={this.props.height} 
              data={this.state.disasters} layout='vertical'
              margin={{ top: 5, right: 100, bottom: 20, left: 50 }}
            >
              <CartesianGrid 
                strokeDasharray="3 3"
                stroke="#555555" 
                horizontal={false}
                // verticalFill={['#555555']}
                // strokeDasharray="5 5" 
              />
              <YAxis type='category' dataKey='name' 
                interval={0} width={200} 
              />
              <XAxis type='number' padding={{'left': 0}}>
                <Label value="Number of Disasters" 
                  offset={-20} position="insideBottom" 
                  fontSize={20}
                />
              </XAxis>
              <Tooltip 
              formatter={(value, name, props) => ( [value, "Total Disasters"] )}
              />\
              <Bar layout="horizontal" dataKey="value" fill="#8884d8" />
            </BarChart>
          </ResponsiveContainer>

          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <h1>Countries</h1>
          <ResponsiveContainer className="scatter" height={1200*0.5 - 20} width='90%'>
            <ScatterChart width={this.props.width} height = {this.props.height}
              margin={{ top: 20, right: 100, bottom: 20*2.5, left: 200 }}
            >
              <CartesianGrid strokeDasharray="3 3" stroke="#555555" />
              <XAxis 
                dataKey="area" name="Area" type="number" 
                scale="log" domain={[1, 'auto']} 
                ticks={
                  Array.apply(0, Array(9)).map(function(_,b) { return Math.pow(10, b); })
                } 
                tickFormatter={this.numberWithCommas}
              >
                <Label value="Area (km squared)" offset={-20} position="insideBottom" fontSize={20} />
              </XAxis>
              <YAxis dataKey="population" name="Population" type="number" 
                scale="log" domain={[1, 'auto']} 
                ticks={
                  Array.apply(0, Array(11)).map(function(_,b) { return Math.pow(10, b); })
                } 
                tickFormatter={this.numberWithCommas}
              >
                <Label value="Population" offset={+80} position="left" fontSize={20} angle={-90} 
                  style={{ textAnchor: 'middle'}}
                />
              </YAxis>
              {/* <ZAxis dataKey="z" range={[64, 144]} name="score" unit="km" /> */}
              <Tooltip 
                cursor={{ stroke: 'red', strokeWidth: 2 }}
                content={< CountryTooltip />}
              />
              <Scatter 
                data={this.state.countries} fill="#8884d8"
              > {
                  this.state.countries.map((c, index) => {
                    return <Cell key={`cell-${index}`} fill={this.state.incomeColors[c.income]} />
                  })
                } </Scatter>
              <Legend 
                layout="horizontal" verticalAlign="top" align="center" 
                payload = {
                  Object.keys(this.state.incomeColors).map((k, index) => {
                    return {
                      'value': k, 'type': 'circle',
                      'id': `ID${index}`, 'color': this.state.incomeColors[k]
                    }
                  })
                }
                height={36}
              >
              </Legend>
            </ScatterChart>
          </ResponsiveContainer>

          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <h1>Organizations</h1>
          <ResponsiveContainer className="pie" height={550} width='100%'>
            <PieChart width={this.props.width} height={this.props.height}
              margin={{ top: 20*2.5, bottom: 20*2.5, left: this.props.width }}>
              <Pie data={this.state.orgs} 
                activeIndex={this.state.activeIndex}
                activeShape={(props) => renderActiveShape(props, 417)}
                cx="50%"
                cy="50%"
                innerRadius={200}
                outerRadius={240}
                dataKey="value"
                onMouseEnter={this.onPieEnter}
              > {
                  this.state.orgs.map((entry, index) => {
                    const colors = ["#666666", "#E69F00", "#56B4E9", "#009E73",
                    "#F0E442", "#0072B2", "#ae0700"];
                    return <Cell key={`cell-${index}`}
                                fill={colors[index]}
                                stroke={undefined}
                            />
                  })
              } </Pie>
            </PieChart>
          </ResponsiveContainer>
          <br></br>
        </container>
      )
    }

}

export default Visualizations;
