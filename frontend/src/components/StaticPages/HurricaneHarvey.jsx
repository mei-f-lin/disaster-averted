import React from "react";
import './../App.css';

function HurricaneHarvey() {
  return (
    <div className="HurricaneHarvey">
      <header className="HurricaneHarvey-header my-5">
      <h1>Hurricane Harvey</h1>
      </header>
      <body>
          <center>
            <h3>Type: Hurricane</h3>
            <p>Hurricane Harvey was a devastating Category 4 hurricane that made landfall on Texas and Louisiana in August 2017, causing catastrophic flooding and many deaths. 
            It is tied with 2005's Hurricane Katrina as the costliest tropical cyclone on record, inflicting $125 billion (2017 USD) in damage, primarily from catastrophic rainfall-triggered 
            flooding in the Houston metropolitan area and Southeast Texas; this made the storm the costliest natural disaster recorded in Texas at the time.</p>
              <h3>Hurricane Map</h3>
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Harvey_2017-08-25_2231Z.png/1200px-Harvey_2017-08-25_2231Z.png" alt=""/>
          </center>
        </body>
    </div>
  );
}

export default HurricaneHarvey;
