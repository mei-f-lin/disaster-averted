import React from "react";
import './../App.css';
import './Map.css';

function UnitedStates() {
    return (
        <div className="map">
            <header className="map-header my-5">
            <h1>Disasters in the United States</h1>
            </header>
            <body>
                <center>
                    <h3>Map of the United States</h3>
                    <img src="https://www.nesdis.noaa.gov/sites/default/files/assets/images/infrared_imagery.jpg" alt=""/>
                    <h3>Most Common Disaster</h3>
                    <img src="https://media.nationalgeographic.org/assets/photos/334/101/fb59b2ba-c5ee-4583-a684-71d4d68aba6e.jpg" alt=""/>
                </center>
            </body>
        </div>
    );
}

export default UnitedStates;

