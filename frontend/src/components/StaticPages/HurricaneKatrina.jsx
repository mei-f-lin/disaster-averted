import React from "react";
import './../App.css';

function HurricaneKatrina() {
  return (
    <div className="HurricaneKatrina">
      <header className="HurricaneKatrina-header my-5">
      <h1>Hurricane Katrina</h1>
      </header>
      <body>
          <center>
            <h3>Type: Hurricane</h3>
            <p>Hurricane Katrina was a large Category 5 Atlantic hurricane which caused over 1,800 deaths and $125 billion in damage in August 2005, particularly in the city of New Orleans and the surrounding areas. 
                It was at the time the costliest tropical cyclone on record, and is now tied with 2017's Hurricane Harvey. The storm was the twelfth tropical cyclone, the fifth hurricane, 
                and the third major hurricane of the 2005 Atlantic hurricane season, as well as the fourth-most intense Atlantic hurricane on record to make landfall in the contiguous United States.</p>
              <h3>Hurricane Map</h3>
              <img src="https://cdn.britannica.com/74/121674-050-C458B2B5/satellite-image-National-Oceanic-and-Atmospheric-Administration-August-28-2005.jpg" alt=""/>
          </center>
        </body>
    </div>
  );
}

export default HurricaneKatrina;
