import React from "react";
import './../App.css';
import './Map.css';

function Japan() {
    return (
        <div className="map">
            <header className="map-header my-5">
            <h1>Disasters in Japan</h1>
            </header>
            <body>
                <center>
                    <h3>Map of Japan</h3>
                    <img src="https://www.nasa.gov/images/content/527359main_japan_earthquake_946-710.jpg" alt=""/>
                    <h3>Most Common Disaster</h3>
                    <img src="https://cdn.britannica.com/59/148659-050-E112CD64/Members-operations-recovery-rescue-Japanese-Ground-Self-Defense-March-11-2011.jpg" alt=""/>
                </center>
            </body>
        </div>
    );
}

export default Japan;