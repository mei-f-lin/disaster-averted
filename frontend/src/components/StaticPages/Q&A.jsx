import React from "react";
import './../App.css';

function Q_A() {
  return (
    <div className="Q_A">
      <div class="container">
        <div class="row align-items-center my-5">
          <div class="col-lg-6">
            <h1>Questions and Answers</h1>
            <dl class="faq">
  <dt>
    <button aria-expanded="false" aria-controls="faq1_desc">
      Can this website be used to get advanced warnings for natural disasters?
    </button>
  </dt>
  <dd>
    <p id="faq1_desc" class="desc">
      The purpose of this website is not necessarily to give advanced heads up 
      warnings but more so to educate people about different natural disasters 
      so they can be more prepared.
    </p>
  </dd>
  <dt>
    <button aria-expanded="false" aria-controls="faq2_desc">
      How often is this page being updated?
    </button>
  </dt>
  <dd>
    <p id="faq2_desc" class="desc">
      As of right now, we are updating this page on a weekly basis, but we hope 
      to implement auto updating so that it can be updated more frequently and automated.
    </p>
  </dd>
  <dt>
    <button aria-expanded="false" aria-controls="faq3_desc">
      Will there be ongoing discussions on newer disasters?
    </button>
  </dt>
  <dd>
    <p id="faq3_desc" class="desc">
      Special pages can be added if there is a disaster that has a significant impact on an area.
      We will be sure to highlight this and add what tips we can for that.
    </p>
  </dd>
</dl>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Q_A;
