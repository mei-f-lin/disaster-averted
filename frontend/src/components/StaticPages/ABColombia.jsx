import React from "react";
import './../App.css';

function ABColombia() {
  return (
    <div className="ABCColombia">
      <header className="ABCColombia-header my-5">
        <h1>ABColombia (ABC Group)</h1>
        <img class="mb-2 mb-lg-0" src="https://scontent-dfw5-1.xx.fbcdn.net/v/t1.0-9/426077_354210794624378_979892059_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=7Zsy55UKoUIAX-2Y7Bs&_nc_ht=scontent-dfw5-1.xx&oh=8942ceb7ca39bdbce728136f8f151a08&oe=6069E6F7"/>
      </header>
      <body>
          <center>
            <h3>Organization Type: Non-Governmental</h3>
            <h3>Headquarters: UK and Northern Ireland</h3>
            <h3>Homepage: <a href="http://www.abcolombia.org.uk/">http://www.abcolombia.org.uk/</a></h3>
            <h3>Address: Romero House, 55 Westminster Bridge Road, London SE1 7JB</h3>
            <h3>Phone: +44 (0)207 870 2217</h3>
            <h3>Email: abcolombia@abcolombia.org.uk</h3>
            <br></br>
            <p> "ABColombia’s purpose to influence decision makers in the UK and Ireland and through them, international/intergovernmental level decision making with a view to resolving the human rights and humanitarian crisis in Colombia and achieve a lasting peace and equitable and sustainable development."</p>
          </center>
        </body>
    </div>
  );
}

export default ABColombia;