import React from "react";
import './../App.css';

function Image() {
  return (
    <div className="images-videos">
      <div class="container">
        <div class="row my-5 pb-5">
          <div class="col-lg-5 my-4">
            <img
              class="img-fluid mb-4 mb-lg-0"
              src="https://media.istockphoto.com/videos/wild-fire-video-id1045133462?s=640x640"
              alt=""
            />
          </div>
          <div class="col-lg-3 my-4">
            <img
              class="img-fluid mb-4 mb-lg-0"
              src="https://static01.nyt.com/images/2020/05/18/science/18CLI-HURRICANES/18CLI-HURRICANES-mediumSquareAt3X.jpg"
              alt=""
            />
          </div>
          <div class="col-lg-4 my-4">
            <img
              class="img-fluid mb-4 mb-lg-0"
              src="https://assets.bwbx.io/images/users/iqjWHBFdfxIU/iJMRWBqJe7WA/v1/-1x-1.jpg"
              alt=""
            />
          </div>
          <div class="col-lg-4 my-4">
            <img
              class="img-fluid mb-4 mb-lg-0"
              src="https://media4.s-nbcnews.com/j/newscms/2018_49/2669406/181204-japan-tsunami-earthquake-cs-920a_075a953d76eb5447a6bf4fd422e45244.fit-760w.jpg"
              alt=""
            />
          </div>
          <div class="col-lg-4 my-4">
            <img
              class="img-fluid mb-4 mb-lg-0"
              src="https://cdn.downtoearth.org.in/library/large/2019-09-12/0.80168200_1568290445_id-twitter.jpg"
              alt=""
            />
          </div>
          <div class="col-lg-4 my-4">
            <img
              class="img-fluid mb-4 mb-lg-0"
              src="https://upload.wikimedia.org/wikipedia/commons/9/98/F5_tornado_Elie_Manitoba_2007.jpg"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Image;
