import React from "react";
import './../App.css';

function AAN() {
  return (
    <div className="AAN">
      <header className="AAN-header my-5">
        <h1>AAN Associates</h1>
        <img class="mb-2 mb-lg-0" src="https://media-exp1.licdn.com/dms/image/C4E1BAQGALFCunGflIA/company-background_10000/0/1547533526255?e=2159024400&v=beta&t=SrRv_bfxsULEEVdtSLy8nB66fG4zB-ggWITpauIyZMI"/>
      </header>
      <body>
          <center>
            <h3>Organization Type: Other</h3>
            <h3>Headquarters: Pakistan</h3>
            <h3>Homepage: <a href="http://aanassociates.com/">http://aanassociates.com/</a></h3>
            <h3>Address: 2nd Floor, Mandeer Square, G-8 Markaz, Islamabad </h3>
            <h3>Phone: +92 51 2340050</h3>
            <h3>Email: info@aanassociates.com</h3>
            <br></br>
            <p> "We are an international development and communication 
              consulting firm providing advisory and implementation services 
              to bi/multilateral donors, UN agencies, public entities, and 
              non-profit organizations."</p>
          </center>
        </body>
    </div>
  );
}

export default AAN;