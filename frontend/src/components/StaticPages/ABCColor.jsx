import React from "react";
import './../App.css';

function ABCColor() {
  return (
    <div className="ABCColor">
      <header className="ABCColor-header my-5">
        <h1>ABC Color</h1>
        <img class="mb-2 mb-lg-0" src="https://www.abc.com.py/pf/resources/images/OG-Image-ABC.png?d=648"/>
      </header>
      <body>
          <center>
            <h3>Organization Type: Media</h3>
            <h3>Headquarters: Paraguay</h3>
            <h3>Homepage: <a href="http://www.abc.com.py/">http://www.abc.com.py/</a></h3>
            <h3>Address:  </h3>
            <h3>Phone: (021) 4151000</h3>
            <h3>Email: abcmedia@abc.com.py</h3>
            <br></br>
            <p> </p>
          </center>
        </body>
    </div>
  );
}

export default ABCColor;