import React from "react";
import './../App.css';

function CaliforniaWildfires() {
  return (
    <div className="CaliforniaWildfires">
      <header className="CaliforniaWildfires-header my-5">
      <h1>California Wildfires</h1>
      </header>
      <body>
          <center>
            <h3>Type: Wildfire</h3>
            <p>The 2020 California wildfire season, part of the 2020 Western United States wildfire season, was a record-setting year of wildfires in California. 
                By the end of the year, 9,639 fires had burned 4,397,809 acres (1,779,730 ha), more than 4% of the state's roughly 100 million acres of land, 
                making 2020 the largest wildfire season recorded in California's modern history (according to the California Department of Forestry and Fire Protection), 
                though roughly equivalent to the pre-1800 levels which averaged around 4.4 million acres yearly and up to 12 million in peak years.</p>
              <h3>Wildfire Map</h3>
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/West_coast_wildfires_ESA22200484.jpeg/596px-West_coast_wildfires_ESA22200484.jpeg" alt=""/>
          </center>
        </body>
    </div>
  );
}

export default CaliforniaWildfires;
