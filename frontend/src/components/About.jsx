import React, { Component } from "react";
import axios from 'axios';
import {Card, CardDeck} from 'react-bootstrap';
import tejas from './AboutPhotos/Tejas.png';
import ashish from './AboutPhotos/ashish.png';
import anvith from './AboutPhotos/anvith.png';
import mei from './AboutPhotos/mei.png';
import sam from './AboutPhotos/sam.png';
class About extends Component {
  constructor(props) {
    // since we are extending class Table so we have to use super in order to override Component class constructor
    super(props)
    this.state = {
      header: ["name", "photo", "bio", "username", "commits", "issues", "unit tests"],
      user_table: [],
      overall_stats: {},
      photo: [mei, sam, ashish, tejas, anvith]
    }
  }

  componentDidMount() {
    this.fetchUserData()
  }

  fetchUserData = async () => {
    const { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/query_users`
    );
    const { user_table, overall_stats } = data;
    this.setState({ user_table, overall_stats });
  }

  renderTableData() {
    return this.state.user_table.map((user, index) => {
      const { name, photo, resp, bio, commits, issues, unitTests } = user // destructuring
      return (
        <Card>
        <Card.Img variant="top" src={photo} />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>
            Bio: {bio}<br></br>
            Responsibilities: {resp}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
        <small className="text-muted">
          Commits: {commits} <br></br> 
          Issues: {issues} <br></br> 
          Tests: {unitTests} <br></br>
        </small>
        </Card.Footer>
      </Card>
      )
    })
 }
  render() {
    const BorderStyle = {paddingTop: '2%', marginTop: '2%', borderTop: '.0625rem dotted black'};
    return (
      <div className = "About">
        <header className="about-header mt-5 mb-4">
          <h1>About Us</h1>
        </header>
          <h5 className='mx-5'>Disaster Averted is a web application that covers natural disasters worldwide. Our mission is to provide more information to the general public about natural disasters and how they can be provided relief.</h5>
          <h1 className='my-4' style={BorderStyle}>Team Members</h1>
          <CardDeck className='mx-3'>
             <Card>
              <Card.Img variant="top" src={this.state.photo[0]} />
              <Card.Body>
                <Card.Title>Meifeng Lin</Card.Title>
                <Card.Text>
                  Bio: Hey I’m Mei! I’m a second year CS major interested in surgical robotics. When I’m not busy with CS, I like to boulder at ABP, play soccer, and hang out with friends. <br></br>
                  Responsibilities: Front-End Developer{''}
                </Card.Text>
              </Card.Body>
              <Card.Footer>
              <small className="text-muted">Commits: {this.state.user_table.map(function (index){return index.commits;})[0]} <br></br> Issues: {this.state.user_table.map(function (index){return index.issues;})[0]}<br></br>Tests: {this.state.user_table.map(function (index){return index.unitTests;})[0]}</small>
              </Card.Footer>
            </Card>
            <Card>
              <Card.Img variant="top" src={this.state.photo[1]} />
              <Card.Body>
                <Card.Title>Yiheng Su</Card.Title>
                <Card.Text>
                  Bio: Yiheng is currently a junior at UT Austin and an aspiring Data Scientists.  In his spare time, Yiheng loves to hit the local arcades and play rhyme games such as Dance Dance Revolution. <br></br>
                  Responsibilities: He’s responsible for implementing the backend database, API, and web application and ensures that the frontend has what it needs to display.{''}
                </Card.Text>
              </Card.Body>
              <Card.Footer>
              <small className="text-muted">Commits: {this.state.user_table.map(function (index){return index.commits;})[1]} <br></br> Issues: {this.state.user_table.map(function (index){return index.issues;})[1]}<br></br>Tests: {this.state.user_table.map(function (index){return index.unitTests;})[1]}</small>
              </Card.Footer>          
            </Card>
            <Card>
              <Card.Img variant="top" src={this.state.photo[2]} />
              <Card.Body>
                <Card.Title>Ashish Kanjiram</Card.Title>
                <Card.Text>
                  Bio: I am a junior computer science major at UT Austin. I am from Houston, TX, and in my free time I like to binge Netflix or hang out with my dance team. <br></br>
                  Responsibilities: Front-End Developer | Phase I Leader{''}
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">Commits: {this.state.user_table.map(function (index){return index.commits;})[2]} <br></br> Issues: {this.state.user_table.map(function (index){return index.issues;})[2]}<br></br>Tests: {this.state.user_table.map(function (index){return index.unitTests;})[2]}</small>
              </Card.Footer>
            </Card>
            <Card>
              <Card.Img variant="top" src={this.state.photo[3]} />
              <Card.Body>
                <Card.Title>Tejas Karuturi</Card.Title>
                <Card.Text>
                  Bio: Tejas is a Junior at UT Austin. He has some experience with web development from his previous internship. Some of his hobbies include programming and playing chess. <br></br>
                  Responsibilities: FrontEnd Developer{''}
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">Commits: {this.state.user_table.map(function (index){return index.commits;})[3]} <br></br> Issues: {this.state.user_table.map(function (index){return index.issues;})[3]}<br></br>Tests: {this.state.user_table.map(function (index){return index.unitTests;})[3]}</small>
              </Card.Footer>
            </Card>
            <Card>
              <Card.Img variant="top" src={this.state.photo[4]} />
              <Card.Body>
                <Card.Title>Anvith Potluri</Card.Title>
                <Card.Text>
                  Bio: Anvith is a sophomore at UT. He is responsible for some of the front-end work that needs to be done. In his free time he enjoys playing basketball and watching tv.<br></br>
                  Responsibilities: Frontend. As a note, some of the commits are not an accurate representation because of the use of VScode liveshare{''}
                </Card.Text>
              </Card.Body>
              <Card.Footer>
              <small className="text-muted">Commits: {this.state.user_table.map(function (index){return index.commits;})[4]} <br></br> Issues: {this.state.user_table.map(function (index){return index.issues;})[4]}<br></br>Tests: {this.state.user_table.map(function (index){return index.unitTests;})[4]}</small>
              </Card.Footer>
            </Card> 
          </CardDeck>
          <h1 className='my-4' style={BorderStyle}>Team Stats</h1>
          <CardDeck className='mx-3'>
            <Card>
              <Card.Body>
                <Card.Title>Total Commits: {this.state.overall_stats['commits']}</Card.Title>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>Total Issues: {this.state.overall_stats['issues']}</Card.Title>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>Total Tests: {46}</Card.Title>
              </Card.Body>
            </Card>
          </CardDeck>
          <h1 className='my-4' style={BorderStyle}>Data Scraping</h1>
          <p className='mx-5 my-0'>
            Data was scraped using Python requests library programmatically from the following sources.
            That is, we simply repeated GET requests to each of the following endpoint.
            Then, to clean and join information together, Pandas was employed. More details can be found in the report.
            Because we are integrating different source datas, it was interesting trying to confirm if attributes 
            describe the same instance from multiple source. For some instances, such as countries, this was easy since
            countries have an unique ISO3 code that's cross platform. For others, such as disasters, this was not so clear. </p>
          <br></br>
          <h10>
            <img
            alt = ""
            src="https://res-2.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco/ebuvt6c16jh9hiq7unso" height={30}/>
            <a href="https://reliefweb.int/help/api">Relief web<br /></a>
          </h10>
          <h10><a href="https://restcountries.eu/rest/v2">REST Countries<br /></a></h10>
          <h10>
            <img
              alt = ""
              src="https://data.worldbank.org/assets/images/logo-wb-header-en.svg" height={30}/>
            <a href="http://api.worldbank.org/v2/country">WorldBank Countries<br /></a>
          </h10>
          <h1 className='my-4' style={BorderStyle}>Tools</h1>
          <img
            alt = ""
            src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height={50}
          />
          <a href="https://about.gitlab.com/">Gitlab: </a><h10>Development platform used for Git version control, pipelines, and issues</h10>
          <br></br>
          <img
            alt = ""
            src="https://user-images.githubusercontent.com/674621/71187801-14e60a80-2280-11ea-94c9-e56576f76baf.png" height={30}
          />
          <a href= "https://code.visualstudio.com/">VSCode</a> <h10> and </h10> <a href="https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare">Live Share Extension: </a> <h10>Text editor and real-time collaboration extension</h10>
          <br></br>
          <img
            alt = ""
            src="https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/267_Python_logo-512.png" height={30}
          />
          <a href="https://www.python.org/">Python and its libraries: </a><h8>Used to construct the backend and data wrangling</h8>
          <br></br>
          <img
            alt = ""
            src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png" height={30}
          />
          <a href="https://reactjs.org/">React: </a><h8>A JavaScript library used to build our application</h8>
          <br></br>
          <img
            alt = ""
            src="https://www.sqlalchemy.org/img/sqla_logo.png" height={30}
          />
          <a href="https://www.sqlalchemy.org/">SQLAlchemy: </a><h8>Backend Database creation and Management</h8>
          <br></br>
          <img
            alt = ""
            src="https://flask.palletsprojects.com/en/1.1.x/_images/flask-logo.png" height={30}
          />
          <a href="https://flask.palletsprojects.com/en/1.1.x/">Flask, Flask-SQLAlchemy, Flask-Restless: </a><h8>Backend web application, 
            connecting to MySQL Database, and RESTful API implementation tool
          </h8>
          <br></br>
          <img
            alt = ""
            src="https://cdn.worldvectorlogo.com/logos/gunicorn.svg" height={30}
          />
          <a href="https://gunicorn.org/">Gunicorn: </a><h8>Deploying Flask web application for production</h8>
          <br></br>
          <img
            alt = ""
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1280px-Amazon_Web_Services_Logo.svg.png" height = {30}
          />
          <a href="https://aws.amazon.com/?nc2=h_lg">AWS: </a><h8>Deployment for frontend and backend</h8>
          <br></br>
          <img
            alt = ""
            src="https://cdn.iconscout.com/icon/free/png-256/namecheap-283654.png" height={30}
          />
          <a href="https://www.namecheap.com/">NameCheap: </a><h8>Domain Routing</h8>
          <br></br>
          <img
            alt = ""
            src="https://miro.medium.com/max/320/0*_rAD9NgK7l6KSlNc.png" height = {30}
          />
          <a href= "https://getbootstrap.com/">Bootstrap: </a> <h10>Open-source toolkit used for front-end styling </h10> 
          <br></br>
          <img
            alt = ""
            src="https://heliocentrix.co.uk/wp-content/uploads/2020/04/microsoft-teams-logo-png_480-480.png" height={30}
          />
          <a href= "https://www.microsoft.com/en-us/microsoft-teams/group-chat-software">Microsoft Teams: </a> <h10>Communication with project team</h10> 
          <br></br>
          <div className='my-4'>
            <h1 style={BorderStyle}>Useful Links</h1>
            <h10><a href="https://gitlab.com/mei-f-lin/disaster-averted">Gitlab Repository<br /></a></h10>
            <h10><a href="https://documenter.getpostman.com/view/14773678/TzCJdoqA">Postman Documentation<br /></a></h10>
            <h10><a href="https://www.youtube.com/watch?v=0tQtGV6mRwM">Video Presentation</a></h10>
          </div>
          <br></br>
      </div>
      
    )
  }
}

export default About;
