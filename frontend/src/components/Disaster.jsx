import React, { Component } from "react";
import {Card, Container, CardDeck, Alert, Row, Col} from 'react-bootstrap';
import axios from 'axios';
import MapContainer from './MapsAPI';
import Typography from '@material-ui/core/Typography';


class Disaster extends Component {
  constructor(props) {
    // since we are extending class Table so we have to use super in order to override Component class constructor
    super(props)
    this.state = {
      attributes: ["Country", "Day", "Glide (Unique ID for cross-platform Identification)", "ID", "Month", "Name", "Status", "Organization", "Type ID", "Weekday", "Year", "Type" ],
      images: ["https://media3.s-nbcnews.com/j/newscms/2020_35/3407271/200825-california-lnu-fire-jm-0957_c9b8238c91004412d2f816fdff345a93.fit-1240w.jpg", "https://www.redzone.co/wp-content/uploads/2018/04/river-flood-1030x687.jpg", "https://www.redrisk.com/redeng/Projects/projects/project12/topimage.jpg", "https://iadsb.tmgrup.com.tr/ed03ed/0/0/0/0/800/518?u=https://idsb.tmgrup.com.tr/2019/01/31/arctic-cold-wave-sends-temps-to-record-lows-in-us-midwest-expected-to-ease-1548943614299.jpg", "https://media.tzuchi.us/wp-content/uploads/2018/11/09152516/ecuadorearthquake2016-4.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBhnM-GByo4Gvz7MurvqxD1Z2vIf7EZ6VezA&usqp=CAU"],
      country: "",
      day: 0,
      glide: "",
      id: 0,
      month: 0,
      name: "",
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      status: "",
      alerts: ['danger', 'warning', 'primary'],
      alertType: -1,
      type: "",
      type_id: "",
      weekday: "",
      year: 0, 
      country_id: 0,
      org_id: 0,
      num : -1,
      s_name: "",

      //for organization card
      org_attributes: ["Name", "Type", "Headquarters", "Status", 
      "Job Openings", "Training Opportunities", "Wrote Reports", "Homepage"],
      organization: "",
      logo_url: "",
      has_job: "",
      has_training: "",
      has_report: "",
      homepage: "",

      //for country card
      country_attributes: ["Capital", "Subregion", "Population", "Income Level"],
      flag: "",
      population: "",
      income: "",
      capital: "",
      subregion: "",
    }
  }
  componentDidMount() {
    this.fetchCountryData()
  }

  fetchCountryData = async () => {
    var num2 = 0
    var link = window.location.href
    var pieces = link.split("/")
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/disasters/` + pieces[pieces.length - 1],
    ); 
    const { country, day, glide, id, month, name, status, type, type_id, weekday, year } = data; //objects stores area, calling codes, etc.
    this.setState({ country, day, glide, id, month, name, status, type, type_id, weekday, year });
    if(this.state.status === "alert") {
      this.state.alertType = 0;
    } else if(this.state.status === "current") {
      this.state.alertType = 1;
    } else {
      this.state.alertType = 2;
    }
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/disasters_link/` + this.state.id,
    );
    const { country_id, org_id } = data;
    this.setState({ country_id, org_id });
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/countries/` + this.state.country_id,
    );
    const { flag, population, income, capital, subregion } = data;
    this.setState({ flag, population, income, capital, subregion });
    var { data } = await axios.get(
      `${process.env.REACT_APP_API_URL}/orgs/` + this.state.org_id,
    );
    if (type_id === "WF") {
      num2 = 0;
    }
    else if (type_id === "FL"){
      num2 = 1;
    }
    else if(type_id === "FF"){
      num2 = 1;
    }
    else if(type_id === "AC"){
      num2 = 2;
    }
    else if(type_id === "CW"){
      num2 = 3;
    }
    else if(type_id === "EQ"){
      num2 = 4;
    }
    else{
      num2 = 5;
    }
    this.state.num = num2;
    const { s_name, logo_url, has_job, has_training, has_report, homepage} = data;
        this.setState({ organization: s_name, logo_url, has_job, has_training, has_report, homepage});
  }

  render() {
    return (
      <div className="Disaster">
        <header className="Disaster-header my-5">
          <h1>{this.state.name}</h1>
          <MapContainer></MapContainer>
          <img 
            src = {this.state.images[this.state.num]} 
            height = {228} 
            width = {'33%'}
            alt = ""
            style={{
              position: 'relative',
              left: '-20%',
              right: '0%',
            }}
          />
        </header>
        <body class="App">
            <center> 
              <Alert variant={this.state.alerts[this.state.alertType]}>
                <Alert.Heading>{this.state.attributes[6]}</Alert.Heading>
                <p className="mb-0">
                  {this.state.status}
                </p>
              </Alert>
              <div>
              <Container>
                <Row className='my-5'>
                    <Col>
                      <h5><b>{this.state.attributes[11]}</b></h5>
                      <h5>{this.state.type}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[8]}</b></h5>
                      <h5>{this.state.type_id}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[0]}</b></h5>
                      <h5>{this.state.country}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[2]}</b></h5>
                      <h5>{this.state.glide}</h5>
                    </Col>
                </Row>
                <Row className='my-5'>
                    <Col>
                      <h5><b>{this.state.attributes[9]}</b></h5>
                      <h5>{this.state.weekday}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[1]}</b></h5>
                      <h5>{this.state.day}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[4]}</b></h5>
                      <h5>{this.state.months[this.state.month - 1]}</h5>
                    </Col>
                    <Col>
                      <h5><b>{this.state.attributes[10]}</b></h5>
                      <h5>{this.state.year}</h5>
                    </Col>
                </Row>
              </Container>
              </div>
              <CardDeck style={{ width: '80%' }}>
                <Card>
                  <a style={{ cursor: 'pointer', color: 'black'}} href ={"/organizations/" + this.state.org_id}>
                  <Card.Img variant="top" src={this.state.logo_url} height={260} />
                  <Card.Body>
                    <Card.Title>
                    <b>{this.state.attributes[7]}: {this.state.organization}</b> <br></br>
                    </Card.Title>
                    <Typography>
                      {this.state.org_attributes[7]}: <a href= {this.state.homepage}>{this.state.homepage}</a>
                    </Typography>
                    <Typography>
                      {this.state.org_attributes[3]}: {this.state.status}
                    </Typography>
                    <Typography>
                      {this.state.org_attributes[4]}: {this.state.has_job}
                    </Typography>
                    <Typography>
                      {this.state.org_attributes[5]}: {this.state.has_training}
                    </Typography>
                    <Typography>
                      {this.state.org_attributes[6]}: {this.state.has_report}
                    </Typography>
                  </Card.Body>
                  </a>
                </Card>
                <Card>
                  <a style={{ cursor: 'pointer', color: 'black'}} href ={"/countries/" + this.state.country_id}>
                  <Card.Img variant="top" src={this.state.flag} height={260} />
                  <Card.Body>
                    <Card.Title>
                    <b>{this.state.attributes[0]}: {this.state.country}</b>
                    </Card.Title>
                    <Typography>
                      {this.state.country_attributes[0]}: {this.state.capital}
                    </Typography>
                    <Typography>
                      {this.state.country_attributes[1]}: {this.state.subregion}
                    </Typography>
                    <Typography>
                      {this.state.country_attributes[2]}: {this.state.population}
                    </Typography>
                    <Typography>
                      {this.state.country_attributes[3]}: {this.state.income}
                    </Typography>
                  </Card.Body>
                  </a>
                </Card>
              </CardDeck>      
              <br></br>
            </center>
          </body>
      </div>
    );
  }
}

export default Disaster;