import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import axios from 'axios';

export class MapContainer extends Component {
  constructor(props) {
    // since we are extending class Table so we have to use super in order to override Component class constructor
    super(props)
    this.state = {
      longitude: 50,
      latitude: 50,
      id: 0,
      country_id: 0, 
    }
    this.mapStyles = {
      width: '33%',
      height: '33%',
      position: 'absolute',
      left: '55%',
      right: '50%',
    };
  }

  componentDidMount() {
    this.fetchCountryData()
  }

  fetchCountryData = async () => {
    var link = window.location.href
    var pieces = link.split("/")
    if(pieces[pieces.length - 2] === "countries") {
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/countries/` + pieces[pieces.length - 1],
      );
      const { latitude, longitude, country_id } = data;
      this.setState({ latitude, longitude, country_id });
    } else if(pieces[pieces.length - 2] === "disasters") {
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/disasters/` + pieces[pieces.length - 1],
      );
      const { id } = data;
      this.setState({id });
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/disasters_link/` + this.state.id,
      );
      const { country_id } = data;
      this.setState({ country_id });
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/countries/` + this.state.country_id,
      );
      const { latitude, longitude } = data;
      this.setState({ latitude, longitude }); 
    } else {
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/orgs/` + pieces[pieces.length - 1],
      );
      const { id } = data;
      this.setState({id });
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/orgs_link/` + this.state.id,
      );
      const { country_id } = data;
      this.setState({ country_id });
      var { data } = await axios.get(
        `${process.env.REACT_APP_API_URL}/countries/` + this.state.country_id,
      );
      const { latitude, longitude } = data;
      this.setState({ latitude, longitude }); 
    }
  }
  render() {
    const lat = this.state.latitude;
    const lng = this.state.longitude;
    return (
      <div>
        <Map
          google={this.props.google}
          zoom={5}
          style={this.mapStyles}
          center={{lat, lng}}
          position={{lat, lng}}
        >
          <Marker
            position={{lat, lng}}
          ></Marker>
        </Map>
      </div>
      
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyCtzFUIca1C4J-2dHyHdCb6v5Jd7XdlhuA'
})(MapContainer);