import React from "react"
import Typography from '@material-ui/core/Typography';
import {Card, CardColumns, Container} from 'react-bootstrap';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, SearchBox, Highlight, connectHits, Index } from 'react-instantsearch-dom';

function Search(props) { 

    let q = props.match.params.q

    const searchClient = algoliasearch('QXM7PDJTL9', '3ac8cb85497bd7f62735a3645d440ad1');


const CountryHits = ({ hits }) => (
    hits.length === 0 ? (<div><h4>No Results</h4></div>) : 
    <Container fluid className="fullclick">
        <CardColumns>
            {hits.map((hit) => (
                <a style={{ cursor: 'pointer', color: 'black'}} href ={"/countries/" + hit.id}>
                    <Card style={{ width: '24rem' }}>
                        <Card.Body>
                            <Typography gutterBottom variant="h5" component="h2">
                                <Highlight attribute={"name"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>State { hits.length } :&nbsp; </b>
                                <Highlight attribute={"status"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Income:&nbsp;  </b>
                                <Highlight attribute={"income"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Area:&nbsp; </b>
                                <Highlight attribute={"area"} 
                                            tagName="mark" 
                                            hit={hit} />
                                        
                            </Typography>
                            <Typography align="left">
                                <b>Population:&nbsp; </b>
                                <Highlight attribute={"population"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Longitude:&nbsp; </b>
                                <Highlight attribute={"longitude"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Latitude:&nbsp; </b>
                                <Highlight attribute={"latitude"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Calling Code:&nbsp; </b>
                                <Highlight attribute={"callingCodes"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Capital:&nbsp; </b>
                                <Highlight attribute={"capital"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Region:&nbsp; </b>
                                <Highlight attribute={"region"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Subregion:&nbsp; </b>
                                <Highlight attribute={"subregion"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Timezone:&nbsp; </b>
                                <Highlight attribute={"timezone"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>ISO3:&nbsp; </b>
                                <Highlight attribute={"iso3"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>ISO2:&nbsp; </b>
                                <Highlight attribute={"income_iso2"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                        </Card.Body>
                    </Card>
                </a>
            ))}
        </CardColumns>
    </Container>
);


const OrganizationHits = ({ hits }) => (
    hits.length === 0 ? (<div><h4>No Results</h4></div>) :
    <Container fluid className="fullclick">
        <CardColumns>
            {hits.map((hit) => (
                <a style={{ cursor: 'pointer', color: 'black'}} href ={"/organizations/" + hit.id}>
                    <Card style={{ width: '24rem' }}>
                        <Card.Body>
                            <Typography gutterBottom variant="h5" component="h2">
                                <Highlight attribute={"name"} 
                                            tagName="mark" 
                                            hit={hit} />

                            </Typography>
                            <Typography align="left">
                                <b>Type:&nbsp;</b>
                                <Highlight attribute={"type"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Status:&nbsp;</b>
                                <Highlight attribute={"status"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Job Opportunities:&nbsp;</b>
                                <Highlight attribute={"has_job"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Training Opportunities:&nbsp;</b>
                                <Highlight attribute={"has_training"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Wrote Reports:&nbsp;  </b>
                                <Highlight attribute={"has_report"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Homepage:&nbsp;  </b>
                                <Highlight attribute={"homepage"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>ID:&nbsp;  </b>
                                <Highlight attribute={"id"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Short Name:&nbsp;  </b>
                                <Highlight attribute={"s_name"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Jobs:&nbsp;  </b>
                                <Highlight attribute={"jobs"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Reports:&nbsp;  </b>
                                <Highlight attribute={"trainings"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Trainings:&nbsp;  </b>
                                <Highlight attribute={"reports"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                        </Card.Body>
                    </Card>
                </a>
            ))}
        </CardColumns>
    </Container>
);

const DisasterHits = ({ hits }) => (
    hits.length === 0 ? (<div><h4>No Results</h4></div>) :
    <Container fluid className="fullclick">
        <CardColumns>
            {hits.map((hit) => (
                <a style={{ cursor: 'pointer', color: 'black'}} href ={"/disasters/" + hit.id}>
                    <Card style={{ width: '24rem' }}>
                        <Card.Body>
                            <Typography gutterBottom variant="h5" component="h2">
                                <Highlight attribute={"name"} 
                                            tagName="mark" 
                                            hit={hit} />

                            </Typography>
                            <Typography align="left">
                                <b>Type:&nbsp; </b>
                                <Highlight attribute={"type"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Month:&nbsp;  </b>
                                <Highlight attribute={"month"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Day:&nbsp;  </b>
                                <Highlight attribute={"day"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Year:&nbsp;</b>
                                <Highlight attribute={"year"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Weekday:&nbsp;  </b>
                                <Highlight attribute={"weekday"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Country:&nbsp;  </b>
                                <Highlight attribute={"country"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Status:&nbsp;  </b>
                                <Highlight attribute={"status"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Glide:&nbsp;  </b>
                                <Highlight attribute={"glide"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>ID:&nbsp;  </b>
                                <Highlight attribute={"id"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                            <Typography align="left">
                                <b>Type ID:&nbsp;  </b>
                                <Highlight attribute={"type_id"} 
                                            tagName="mark" 
                                            hit={hit} />
                            </Typography>
                        </Card.Body>
                    </Card>
                </a>
            ))}
        </CardColumns>
    </Container>
);

const DisplayCountryHits = connectHits(CountryHits);
const DisplayOrganizationHits = connectHits(OrganizationHits);
const DisplayDisasterHits = connectHits(DisasterHits);

return (
    <div className="content">
        <br></br>
        <h1>Search Results</h1>
        <br></br>
        <br></br>
        <main >
            < InstantSearch searchClient={searchClient} indexName="countries" searchState={{ query: q, }} >
                <div style={{ display: "none" }}><SearchBox /></div>
                <h3><b>Countries</b></h3>
                <Index indexName="countries" >
                    <DisplayCountryHits />
                </Index>
                <br></br>
                <br></br>
                <h3><b>Organizations</b></h3>
                <Index indexName="organizations" >
                    <DisplayOrganizationHits />
                </Index> 
                <br></br>
                <br></br>
                <h3><b>Disasters</b></h3>
                <Index indexName="disasters" >
                    <DisplayDisasterHits />
                </Index>
                <br></br>
                <br></br>
            </InstantSearch>
        </main >
    </div>
);
}
export default Search;
