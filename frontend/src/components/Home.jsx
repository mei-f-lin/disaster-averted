import React, { Component } from 'react';
import {Container, Carousel} from 'react-bootstrap';
import damage from './HomePhotos/damage.png';
import fire from './HomePhotos/fire.png';
import flood from './HomePhotos/flood.png';
import hurricane1 from './HomePhotos/hurricane1.png';
import hurricane2 from './HomePhotos/hurricane2.png';
import rescue1 from './HomePhotos/rescue1.png';
import rescue2 from './HomePhotos/rescue2.png';
import snowstorm from './HomePhotos/snowstorm.png';
import tsunami from './HomePhotos/tsunami.png';
import disasters from './HomePhotos/disasters.gif';
import countries from './HomePhotos/countries.gif';
import organizations from './HomePhotos/organizations.gif';
import './Home.css';

class Home extends Component {

  render() {
    var pics = [fire, flood, hurricane2, snowstorm, tsunami, damage, rescue1, rescue2, hurricane1];
    return (
      <div>
        <Carousel 
          fade = {true}
          pause = {false}
          controls = {false}
          indicators = {false}
          interval = {2000}
        >
          {pics.map((pic) =>(
            <Carousel.Item>
              <div>
                <img
                  class = "format"
                  src={pic}
                  alt = ""
                />
                <div class = "header">
                  <div className="backing"></div>
                </div>
                <div class = "header">
                  <h1 class="title">Disaster Averted</h1>
                  <p class="subtitle"> Click the animations below to learn more about natural disasters and how to help impacted communities!</p>
                </div>
                <div class = "disaster-position">
                  <Container className='hoverable' style={{borderRadius: '8%', padding:0}}>
                    <a href="/disasters">
                        <img className="model-pic" src={disasters} alt = ""></img>
                    </a>
                    <br></br>
                    <p class = "model-title">Disasters</p>
                  </Container>
                </div>
                <div class = "country-position">
                  <Container className='hoverable' style={{borderRadius: '8%', padding:0}}>
                    <a href="/countries">
                        <img className="model-pic" src={countries} alt = ""></img>
                    </a>
                    <br></br>
                    <p class = "model-title">Countries</p>
                  </Container>
                </div>
                <div class = "organization-position">
                  <Container className='hoverable' style={{borderRadius: '8%', padding:0}}>
                    <a href="/organizations">
                        <img className="model-pic" src={organizations} alt = ""></img>
                    </a>
                    <br></br>
                    <p class = "model-title">Organizations</p>
                  </Container>
                </div>
              </div>
            </Carousel.Item>
          ))}
        </Carousel>
      </div>
    );
  }
}

export default Home;
