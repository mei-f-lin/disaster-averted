export function CountryTooltip({ active, payload }) {
    if (active && payload && payload.length) {
      return (
        <div className="custom-tooltip">
          <p className="labelsTop">{`${payload[0].payload.name}`}</p>
          {payload.map((e) => {
            const {name, value } = e
            //const obj = e.payload;
            return (
              <div>
                <p className="labels">
                  {`${name} : ${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`}
                </p>
              </div>
            );
          })}
          <p className="labelsBottom">{`${payload[0].payload.income}`}</p>
        </div>
      );
    }
    // not ready yet, but will over overriden once ready
    return <div>Loading...</div>;
}

export function ProviderCountryTooltip({ active, payload }) {
  if (active && payload && payload.length) {
    console.log(payload);
    return (
      <div className="custom-tooltip">
        <p className="labelsTop">{`${payload[0].payload.name}`}</p>
        {payload.map((e) => {
          const {name, value } = e
          //const obj = e.payload;
          return (
            <div>
              <p className="labels">
                {`${name} : ${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`}
              </p>
            </div>
          );
        })}
        <p className="labelsBottom">{`Region: ${payload[0].payload.region}`}</p>
      </div>
    );
  }
  // not ready yet, but will over overriden once ready
  return <div>Loading...</div>;
}
