import React from "react";
import './../App.css';
import './Map.css';

function Map() {
  return (
    <div className="map">
      <header className="map-header my-5">
      <h1>Maps of Disasters</h1>
      </header>
      <body>
          <center>
              <h3>Hurricane Map</h3>
              <img src="http://www.nasa.gov/images/content/313775main_global_hurr_tracks_HI.jpg" alt=""/>
              <h3>Earthquake Map</h3>
              <img src="https://volcanodiscovery.de/uploads/pics/quakes-15052020.jpg" alt=""/>
              <h3>Tornado Map</h3>
              <img src="https://s.w-x.co/world_tornado.jpg" alt=""/>
              <h3>Wildfire Map</h3>
              <img src="https://lance.modaps.eosdis.nasa.gov/imagery/firemaps/small_current_firemap.jpg" alt=""/>
          </center>
        </body>
    </div>
  );
}

export default Map;
