import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { 
  About, Visualizations, Home, Countries, Country,
  Organizations, Organization, Disasters, Disaster,
  Navigation, Search, ProviderVisualizations
} from "./components";

function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={() => <Home />} />
          <Route path="/about" exact component={() => <About />} />

          <Route exact path="/disasters" component={Disasters} />
          <Route path="/disasters/:id" component={() => <Disaster />} />

          <Route exact path="/countries" component={Countries} />
          <Route path="/countries/:id" component={() => <Country />} />

          <Route path="/organizations" exact component={() => <Organizations />} />
          <Route path="/organizations/:id" component={() => <Organization />} />

          <Route path="/visualizations" component={() => <Visualizations />} />
          <Route path="/providerVisualizations" component={() => <ProviderVisualizations />} />
          <Route path="/search/q=:q" component={Search} />

        </Switch>
      </Router>
    </div>
  );
}

export default App;
