import unittest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from webdriver_manager.chrome import ChromeDriverManager


class Tests(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-uage')
        # chrome_options.add_argument('--disable-setuid-sandbox')
        chrome_options.add_argument('--headless')
        # chrome_options.add_argument('--disable-extensions')
        # chrome_options.add_argument('start-maximized')
        # self.driver = webdriver.Chrome("./chromedriver", options=chrome_options)
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        self.driver.implicitly_wait(10)

    def test_case_1(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me/")
            button = self.driver.find_element_by_xpath("/html/body/div/div/nav/div/div/a[3]")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/disasters", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_2(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me/")
            button = self.driver.find_element_by_xpath("/html/body/div/div/nav/div/div/a[5]")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/Countries", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_3(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me/")
            button = self.driver.find_element_by_xpath("/html/body/div/div/nav/div/div/a[7]")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/organizations", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_4(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me/about")
            button = self.driver.find_element_by_xpath("/html/body/div/div/div/div/h10[1]/a")
            # button = self.driver.find_element_by_name("gitLabLink")
            self.driver.execute_script("arguments[0].click();", button)
            self.driver.implicitly_wait(1000)
            self.assertIn("https://gitlab.com/mei-f-lin/disaster-averted", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_5(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me/about")
            button = self.driver.find_element_by_xpath("/html/body/div/div/div/div/h10[2]/a")
            self.driver.execute_script("arguments[0].click();", button)
            self.driver.implicitly_wait(1000)
            self.assertIn("https://documenter.getpostman.com/view/14773678/TzCJdoqA", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)
        
    def test_case_6(self):
        try:
            self.driver.get("https://disaster-averted.me")
            button = self.driver.find_element_by_xpath("/html/body/div/div/div/div/div/div[2]/div/div[3]/div/a")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/disasters", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_7(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me")
            button = self.driver.find_element_by_xpath("/html/body/div/div/div/div/div/div[2]/div/div[4]/div/a")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/countries", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_8(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me")
            button = self.driver.find_element_by_xpath("/html/body/div/div/div/div/div/div[2]/div/div[5]/div/a")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/organizations", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_9(self):
        self.driver.implicitly_wait(10)
        try:
            self.driver.get("https://disaster-averted.me")
            button = self.driver.find_element_by_xpath("/html/body/div/div/nav/div/div/a[1]")
            self.driver.execute_script("arguments[0].click();", button)
            self.assertIn("https://disaster-averted.me/about", self.driver.current_url)
        except NoSuchElementException as ex:
            self.fail(ex.msg)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Tests)
    result = unittest.TextTestRunner(verbosity=2).run(suite)

    if result.wasSuccessful():
        exit(0)
    else:
        exit(1)
