echo "Deploying Backend..."
cd backend
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 306683688142.dkr.ecr.us-east-2.amazonaws.com
docker build -t disaster-averted-backend .
docker tag disaster-averted-backend:latest 306683688142.dkr.ecr.us-east-2.amazonaws.com/disaster-averted-backend:latest
docker push 306683688142.dkr.ecr.us-east-2.amazonaws.com/disaster-averted-backend:latest
cd aws_deploy
eb deploy
