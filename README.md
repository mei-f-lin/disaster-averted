# Disaster Averted

A website to get more informed about natural disasters and learn how to help impacted communities!

## Website
https://disaster-averted.me

## Postman Documentation
https://documenter.getpostman.com/view/14773678/TzCJdoqA

## Presentation
https://www.youtube.com/watch?v=0tQtGV6mRwM

## Team Members

| Name | EID | GitLabID |
|---|---|---|
| Ashish Kanjiram | ajk3367  | @akanjiram  |
| Yiheng Su  | ys22933  | @sam.su1  |
| Meifeng Lin  | ml48527  | @mei-f-lin  |
| Tejas Karuturi  | tk22543  | @tkaruturi  |
| Anvith Potluri  | akp2597  | @anvithpotluri  | 

## Project Leader
| Phase | Project Leader |
| --- | --- |
| Phase 1 | Ashish Kanjiram |
| Phase 2 | Yiheng Su |
| Phase 3 | Tejas Karuturi |
| Phase 4 | Meifeng Lin |

## Time Estimates/Completion
### Phase 1
| Name | Estimate | Actual |
|---|---|---|
| Ashish Kanjiram | 10 hours  | 14 hours  |
| Yiheng Su  | 10 hours  | 14 hours  |
| Meifeng Lin  | 10 hours  | 14 hours  |
| Tejas Karuturi  | 10 hours  | 14 hours  |
| Anvith Potluri  | 10 hours  |  14 hours  | 

### Phase 2
| Name | Estimate | Actual |
|---|---|---|
| Ashish Kanjiram | 15 hours  | 18 hours  |
| Yiheng Su  | 15 hours  | 22 hours  |
| Meifeng Lin  | 15 hours  | 18 hours  |
| Tejas Karuturi  | 15 hours  | 18 hours  |
| Anvith Potluri  | 15 hours  |  18 hours  | 

### Phase 3
| Name | Estimate | Actual |
|---|---|---|
| Ashish Kanjiram | 14 hours  | 18 hours  |
| Yiheng Su  | 14 hours  | 18 hours  |
| Meifeng Lin  | 14 hours  | 18 hours  |
| Tejas Karuturi  | 14 hours  | 18 hours  |
| Anvith Potluri  | 14 hours  |  18 hours  | 

### Phase 4
| Name | Estimate | Actual |
|---|---|---|
| Ashish Kanjiram | 16 hours  | 20 hours  |
| Yiheng Su  | 16 hours  | 20 hours  |
| Meifeng Lin  | 16 hours  | 20 hours  |
| Tejas Karuturi  | 16 hours  | 20 hours  |
| Anvith Potluri  | 16 hours  |  20 hours  | 

## Gitlab Pipelines
https://gitlab.com/mei-f-lin/disaster-averted/-/pipelines

## Git SHA
| Phase | Git SHA |
| --- | --- |
| Phase 1 | 3279e0b8826c68507a97e5210cc6d626af2f2fe8 |
| Phase 2 | 71c06fb775625b8957eaa4233f3d0890c80e9cf8 |
| Phase 3 | edb4d0f6e525184e247b07aa7953b5acd606b106 |
| Phase 4 | cc92d3c93c320c1f9f26f8debe9aee8c28b2beaf |

## Comments: 

## Backend Database

aws_base = {
    'host': 'disaster-averted-db.ct7kdzdhfbo7.us-east-1.rds.amazonaws.com',
    'dbname': 'disaster_averted_mySQL_db',
    'user': 'disaster214',
    'password': 'aversion469',
    'port': 3306
}

The ER (Entity-Relationship) Diagram has red and blue diamonds representing foreign keys and normal columns respectively. 
Primary keys/columns have the key logo next to them.

## Tutorials:

Navigation Bar and Pages: https://www.techomoro.com/how-to-create-a-multi-page-website-with-react-in-5-minutes/

Deploying Frontend and Backend: https://adamraudonis.medium.com/how-to-deploy-a-website-on-aws-with-docker-flask-react-from-scratch-d0845ebd9da4

Dynamic Tables in React: https://dev.to/abdulbasit313/an-easy-way-to-create-a-customize-dynamic-table-in-react-js-3igg

## Steps for Local Testing:

git clone this repo

cd frontend
npm install
export REACT_APP_API_URL=http://localhost:8080/api (if running backend locally)
export REACT_APP_API_URL=https://disaster-averted.me/api (if using deployed backend app)
npm start

cd backend
python app.py

## Deployment

Backend: 

Assuming you are at /disaster-averted

cd backend/aws_deploy
eb init (region 14, application 1)
cd ..
./deploy_backend.sh

Cannot setup CodeCommit because there is no Source Control setup, continuing with initialization is Ok!

Frontend:

./deploy_frontend.sh
create Invalidation at Cloufront using path for everything: /*
check https://disaster-averted.me to see changes updated