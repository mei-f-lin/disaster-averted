.DEFAULT_GOAL := format
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -p), unknown)
	BLACK         := python -m black
	PYLINT        := python -m pylint
	PYTHON        := python
else
	BLACK         := python3 -m black
	PYLINT        := python3 -m pylint3
	PYTHON        := python3
endif 

PYFILES := `find backend -name "*.py"`

# housekeeping
clean:
	-rm -rf frontend/build
	-rm -rf frontend/node_modules
	-rm frontend/package-lock.json
	-rm -rf backend/__pycache__
	-rm -rf backend/venv

# backend commands
format-backend:
	@for name in $(PYFILES); do $(BLACK) $$name; done

lint-backend:
	@for name in $(PYFILES); do $(PYLINT) $$name; done

# frontend commands
build-frontend:
	cd frontend && npm install && npm run build

# clean and format
pretty:
	make clean
	make format-backend
	make lint-backend

# output versions of all tools
versions:
	@echo  'shell uname -p'
	@echo $(shell uname -p)
	@echo
	@echo  'shell uname -s'
	@echo $(shell uname -s)
	@echo
	@echo "% which $(BLACK)"
	@which $(BLACK)
	@echo
	@echo "% $(BLACK) --version"
	@$(BLACK) --version
	@echo
	@echo "% which $(PYLINT)"
	@which $(PYLINT)
	@echo
	@echo "% $(PYLINT) --version"
	@$(PYLINT) --version
	@echo
	@echo "% which $(PYTHON)"
	@which $(PYTHON)
	@echo
	@echo "% $(PYTHON) --version"
	@$(PYTHON) --version
