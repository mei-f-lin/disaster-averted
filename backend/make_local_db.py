import pandas as pd
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy_utils import database_exists, create_database

# core interface to the database
engine = create_engine("mysql+pymysql://root@mysql/test_db")
if not database_exists(engine.url):
    create_database(engine.url)
# base class which maintains a catalog of classes and tables
Base = declarative_base()

# Let's define classes using the Declarative system, which
# provides a default constructor that automatically initializes
# each field

# Disaster Class/Model
class Disaster(Base):
    """
    Disaster (1915) Attributes:
    """

    __tablename__ = 'disasters'
    id = Column(Integer, primary_key=True)
    type = Column(String(50))
    type_id = Column(String(10))
    name = Column(String(1000))
    glide = Column(String(100))
    status = Column(String(10))
    year = Column(Integer)
    month = Column(Integer)
    day = Column(Integer)
    weekday = Column(String(20))
    country = Column(String(100))

# Country Class/Model
class Country(Base):
    """
    Country (110) Attributes:
    """

    __tablename__ = 'countries'

    id = Column(Integer, primary_key=True)
    name = Column(String(1000))
    longitude = Column(Float)
    latitude = Column(Float)
    status = Column(String(10))
    iso3 = Column(String(3))
    callingCodes = Column(Integer)
    capital = Column(String(50))
    region = Column(String(50))
    subregion = Column(String(50))
    population = Column(Float)
    area = Column(Float)
    timezones = Column(String(1500))
    flag = Column(String(1000))
    income = Column(String(50))
    income_iso2 = Column(String(2))

# Organizations Class/Model
class Organization(Base):
    """
    Organization (467) Attributes:
    """

    __tablename__ = 'orgs'
    id = Column(Integer, primary_key=True)
    type = Column(String(100))
    s_name = Column(String(100))
    name = Column(String(1000))
    status = Column(String(10))
    homepage = Column(String(1000))
    logo_url = Column(String(1000))
    has_training = Column(String(3))
    trainings = Column(Integer)
    has_report = Column(String(3))
    reports = Column(Integer)
    has_job = Column(String(3))
    jobs = Column(Integer)

# Because of the complicated inter-dependency situation
# for each link, we create a separate table (6 in total)
# each table will only have two attributes, an id field
# and <model>_id field which points to one of the above tables

class Disaster_Link(Base):
    """
    Links one instance of disaster to a country
    """

    __tablename__ = "disasters_link"
    id = Column(Integer, primary_key=True) # same as disaster.id
    country_id = Column(Integer, ForeignKey("countries.id"))
    org_id = Column(Integer, ForeignKey("orgs.id"))


class Org_Link(Base):
    """
    Links one instance of an organization to a disaster
    """

    __tablename__ = "orgs_link"
    id = Column(Integer, primary_key=True)  # same as org.id
    disaster_id = Column(Integer, ForeignKey("disasters.id"))
    country_id = Column(Integer, ForeignKey("countries.id"))

class Country_Link(Base):
    """
    Links one instance of country to a disaster
    """

    __tablename__ = "countries_link"
    id = Column(Integer, primary_key=True)  # same as country.id
    disaster_id = Column(Integer, ForeignKey("disasters.id"))
    org_id = Column(Integer, ForeignKey("orgs.id"))

Base.metadata.create_all(engine)

# ORM’s “handle” to the database
Session = sessionmaker(bind=engine)
session = Session()

# we now insert into the established tables
country = pd.read_pickle("./data/final_country_table.pkl")
disaster = pd.read_pickle("./data/final_disaster_table.pkl")
org = pd.read_pickle("./data/final_org_table.pkl")

def make_disaster(row):
    return Disaster(
        id = row['disaster_id'],
        type = row['type'],
        type_id = row['type_id'],
        name = row['name'],
        glide = row['glide'],
        status = row['status'],
        year = row['year'],
        month = row['month'],
        day = row['day'],
        weekday = row['weekday'],
        country = row['country']
    )

def disaster_link(row):
    return Disaster_Link(
        id = row['disaster_id'],
        country_id = row['country_id'],
        org_id = row['org_id']
    )

def make_organization(row):
    return Organization(
        id = row['org_id'],
        type = row['type'],
        s_name = row['s_name'],
        name = row['name'],
        status = row['status'],
        homepage = row['homepage'],
        logo_url = row['logo_url'],
        has_training = row['has_training'],
        trainings = row['trainings'],
        has_report = row['has_report'],
        reports = row['reports'],
        has_job = row['has_job'],
        jobs = row['jobs']
    )

def org_link(row):
    return Org_Link(
        id = row['org_id'],
        disaster_id = row['disaster_id'],
        country_id = row['country_id']
    )

def make_country(row):
    return Country(
        id = row['country_id'],
        name = row['name'],
        longitude = row['longitude'],
        latitude = row['latitude'],
        status = row['status'],
        iso3 = row['iso3'],
        callingCodes = row['callingCodes'],
        capital = row['capital'],
        region = row['region'],
        subregion = row['subregion'],
        population = row['population'],
        area = row['area'],
        timezones = row['timezones'],
        flag = row['flag'],
        income = row['income'],
        income_iso2 = row['income_iso2']
    )

def country_link(row):
    return Country_Link(
        id = row['country_id'],
        disaster_id = row['disaster_id'],
        org_id = row['org_id']
    )

# add the model tables first
session.add_all(
    country.apply(make_country, axis=1).to_list()
)

session.add_all(
    disaster.apply(make_disaster, axis=1).to_list()
)

session.add_all(
    org.apply(make_organization, axis=1).to_list()
)

session.commit()

# then, add the model linking tables
session.add_all(
    country.apply(country_link, axis=1).to_list()
)

session.add_all(
    disaster.apply(disaster_link, axis=1).to_list()
)

session.add_all(
    org.apply(org_link, axis=1).to_list()
)

session.commit()
session.close()
