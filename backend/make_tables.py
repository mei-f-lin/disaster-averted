"""
AWS automatically creates the database schema `disaster_averted_mySQL_db`
inside the MySQL database managed by AWS RDS. Hence, when we establish the
connection, we connect to that databse specifically

In this file, we create three tables, one per each model, defined here by
three individual relation schemas per model (ie. blue print for features).
In addition, three more tables are created after the creation of the model
tables, specifically for linking purposes.
For simplicitly, each column/feature is placed on a separate line like:

`name` <type> modifiers

Types Used:
* STRING(n): string up to length n
* INTEGER

For complete list see: https://dev.mysql.com/doc/refman/5.6/en/data-types.html
"""

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float

# core interface to the database
engine = create_engine(
    'mysql+pymysql://disaster214'
    ':aversion469@disaster-averted-db'
    '.ct7kdzdhfbo7.us-east-1.'
    'rds.amazonaws.com/disaster_averted_mySQL_db')
# base class which maintains a catalog of classes and tables
Base = declarative_base()

# Let's define classes using the Declarative system, which
# provides a default constructor that automatically initializes
# each field

# Disaster Class/Model
class Disaster(Base):
    """
    Disaster (1915) Attributes:
    """

    __tablename__ = 'disasters'
    id = Column(Integer, primary_key=True)
    type = Column(String(50))
    type_id = Column(String(10))
    name = Column(String(100))
    glide = Column(String(100))
    status = Column(String(10))
    year = Column(Integer)
    month = Column(Integer)
    day = Column(Integer)
    weekday = Column(String(20))
    country = Column(String(100))

# Country Class/Model
class Country(Base):
    """
    Country (110) Attributes:
    """

    __tablename__ = 'countries'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    longitude = Column(Float)
    latitude = Column(Float)
    status = Column(String(10))
    iso3 = Column(String(3))
    callingCodes = Column(Integer)
    capital = Column(String(50))
    region = Column(String(50))
    subregion = Column(String(50))
    population = Column(Float)
    area = Column(Float)
    timezones = Column(String(100))
    flag = Column(String(1000))
    income = Column(String(50))
    income_iso2 = Column(String(2))

# Organizations Class/Model
class Organization(Base):
    """
    Organization (467) Attributes:
    """

    __tablename__ = 'orgs'
    id = Column(Integer, primary_key=True)
    type = Column(String(100))
    s_name = Column(String(100))
    name = Column(String(100))
    status = Column(String(10))
    homepage = Column(String(1000))
    logo_url = Column(String(1000))
    has_training = Column(String(3))
    trainings = Column(Integer)
    has_report = Column(String(3))
    reports = Column(Integer)
    has_job = Column(String(3))
    jobs = Column(Integer)

# Because of the complicated inter-dependency situation
# for each link, we create a separate table (6 in total)
# each table will only have two attributes, an id field
# and <model>_id field which points to one of the above tables

class Disaster_Link(Base):
    """
    Links one instance of disaster to a country
    """

    __tablename__ = "disasters_link"
    id = Column(Integer, primary_key=True) # same as disaster.id
    country_id = Column(Integer, ForeignKey("countries.id"))
    org_id = Column(Integer, ForeignKey("orgs.id"))


class Org_Link(Base):
    """
    Links one instance of an organization to a disaster
    """

    __tablename__ = "orgs_link"
    id = Column(Integer, primary_key=True)  # same as org.id
    disaster_id = Column(Integer, ForeignKey("disasters.id"))
    country_id = Column(Integer, ForeignKey("countries.id"))

class Country_Link(Base):
    """
    Links one instance of country to a disaster
    """

    __tablename__ = "countries_link"
    id = Column(Integer, primary_key=True)  # same as country.id
    disaster_id = Column(Integer, ForeignKey("disasters.id"))
    org_id = Column(Integer, ForeignKey("orgs.id"))

Base.metadata.create_all(engine)
