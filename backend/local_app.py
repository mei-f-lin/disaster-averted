import requests
from flask import Flask, request
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from make_tables import Disaster, Country, Organization
from make_tables import Disaster_Link, Org_Link, Country_Link
from flask_cors import CORS

# Creating Flask Application
app = Flask(__name__)
CORS(app)

# Connecting MYSQL Database
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'mysql+pymysql://root@mysql/test_db'
)

# track modifications of objects and emit signals,
# This requires extra memory and should be disabled if not needed.
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['API_PREFIX'] = '/api'

db = SQLAlchemy(app)

# Create the Flask-Restless API manager.
manager = APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
# It actually returns a blueprint, but not actually necessary since it's
# already registered

# This creates the get API endpoints for each table
# for specifics, see the PostMan API Documentation linked

manager.create_api(Disaster, methods=['GET'])
manager.create_api(Country, methods=['GET'])
manager.create_api(Organization, methods=['GET'])
manager.create_api(Disaster_Link, methods=['GET'])
manager.create_api(Org_Link, methods=['GET'])
manager.create_api(Country_Link, methods=['GET'])

# Further endpoints relied by the frontend and AWS

@app.route('/api')
def home():
    return request.base_url

@app.route('/api/query_users')
def query_users():
    # disconnect between commit metadata and gitlab user metadata, 
    # make looktable from email to gitlab username
    get_gitlab_name = {
        "mei.f.lin@utexas.edu": "mei-f-lin",
        "tkaruturi@yahoo.com": "tkaruturi",
        "tkaruturi@utexas.edu": "tkaruturi",
        "sam.su@utexas.edu": "Sam.Su1",
        "ashkanjiram@hotmail.com": "akanjiram",
        "akanjiram@ashishs-mbp.home": "akanjiram",
        "akanjiram@Ashishs-MacBook-Pro.local": "akanjiram",
        "anvithguppy@gmail.com": "anvithpotluri"
    }
    commit_count = {}
    # extract commit information
    endpoint_commits_base = (
        'https://gitlab.com/api/v4/projects/24742557/repository/commits'
        '?per_page=100&page={page}'
    )
    # we know we have ~200 commits
    for i in range(1, 5):
        url = endpoint_commits_base.format(page=i)
        resp_commits = requests.get(url)

        for commit in resp_commits.json():
            # increment # of commits per unique gitlab name correctly
            gitlab_name = get_gitlab_name[commit['author_email']]
            if not gitlab_name in commit_count:
                commit_count[gitlab_name] = 0
            commit_count[gitlab_name] += 1

    # extract user information
    endpoint_members = 'https://gitlab.com/api/v4/projects/24742557/members'
    headers = {"Authorization": "Bearer 7EP1ebrzspAyn16eN7pE"}
    resp_members = requests.get(endpoint_members, headers=headers)

    user_table = []
    overall_stats = {'commits':0, 'issues':0}
    if resp_members.status_code != 200:
        # This means something went wrong.
        return {'Fucked up': resp_members.status_code}
    for person in resp_members.json():
        if person['access_level'] >= 40:
            # grad the issues made by this user
            user = person['username']
            endpoint_issues = f'https://gitlab.com/api/v4/projects/24742557/issues?author_username={user}'
            resp_issues = requests.get(endpoint_issues, headers=headers)

            # create the table for the user
            user_table.append({
                "name": person['name'],
                "username": person['username'],
                "issues": len(resp_issues.json()),
                "commits": commit_count[person['username']]
            })

            # collect overall stats
            overall_stats['commits'] += commit_count[person['username']]
            overall_stats['issues'] += len(resp_issues.json())

    return {'user_table': user_table, 'overall_stats': overall_stats}

if __name__ == '__main__':
    app.run(debug=True, port=8080)
