FROM ubuntu
RUN apt-get update
RUN apt-get -y install apt-utils
RUN apt-get -y install python3.8
RUN apt-get -y install python3-pip
RUN apt-get -y install mysql-server
RUN apt-get -y install libmariadbclient-dev
RUN apt-get -y install libmysqlclient-dev
WORKDIR /backend
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
