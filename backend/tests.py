#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable=R0904

import ast
from unittest import main, TestCase
from local_app import app

class TestBackend(TestCase):
    def setUp(self):
        self.client = app.test_client()
        self.names = [
            "Yiheng Su", "Mei Lin", "Ashish Kanjiram",
            "Tejas Karuturi", "anvithpotluri"
        ]
        self.usernames = [
            "mei-f-lin", "Sam.Su1", "akanjiram",
            "tkaruturi", "anvithpotluri"
        ]
        self.disaster_info = [
            'country', 'day', 'glide', 'id', 'month', 'name',
            'status', 'type', 'type_id', 'weekday', 'year'
        ]
        self.disaster_link_info = ['country_id', 'id', 'org_id']

        self.country_info = [
            'area', 'callingCodes', 'capital', 'flag', 'id', 'income',
            'income_iso2', 'iso3', 'latitude', 'longitude', 'name',
            'population', 'region', 'status', 'subregion', 'timezones'
        ]
        self.country_link_info = ['disaster_id', 'id', 'org_id']

        self.org_info = [
            'has_job', 'has_report', 'has_training', 'homepage', 'id',
            'jobs', 'logo_url', 'name', 'reports', 's_name', 'status',
            'trainings', 'type'
        ]
        self.org_link_info = ['country_id', 'disaster_id', 'id']

    def validate_user_data(self, user):
        self.assertIn("commits", user)
        self.assertGreater(user['commits'], 0)
        self.assertIn("name", user)
        self.assertIn(user['name'], self.names)
        self.assertIn("issues", user)
        self.assertGreater(user['issues'], 0)
        self.assertIn("username", user)
        self.assertIn(user['username'], self.usernames)

    # in the gitlab continuous integreation, a local database
    # and a local flask app has been set up
    def test_get_users(self):
        resp = self.client.get("/api/query_users")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        user_data = ast.literal_eval(dict_str)
        # make sure expected attributes are there
        self.assertIn("overall_stats", user_data)
        self.assertIn("user_table", user_data)

        # check overall stats
        self.assertGreater(user_data['overall_stats']['commits'], 100)
        self.assertGreater(user_data['overall_stats']['issues'], 30)

        # check usertable
        for user in user_data['user_table']:
            self.validate_user_data(user)

    def validate_count_and_pages(self, data, against):
        self.assertIn("num_results", data)
        self.assertEqual(data['num_results'], against['num_results'])
        self.assertIn("page", data)
        self.assertEqual(data['page'], against['page'])
        self.assertIn("total_pages", data)
        self.assertEqual(data['total_pages'], against['total_pages'])
        self.assertIn("objects", data)
        self.assertGreaterEqual(len(data['objects']), 0)

    def validate_one_instance_values(self, o, against):
        for key in o:
            self.assertEqual(o[key], against[key])

    def validate_one_instance_attributes(self, o, against):
        for attribute in against:
            self.assertIn(attribute, o)

    def test_get_disasters(self):
        resp = self.client.get("/api/disasters")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        disasters_data = ast.literal_eval(dict_str)
        # check headers
        self.validate_count_and_pages(disasters_data, {
            "num_results": 2386,
            "page": 1,
            "total_pages": 239
        })
        # check content
        for o in disasters_data['objects']:
            self.validate_one_instance_attributes(o, self.disaster_info)

    def test_get_disasters_one(self):
        resp = self.client.get("/api/disasters/4613")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        disasters_data = ast.literal_eval(dict_str)
        # check content
        self.validate_one_instance_attributes(
            disasters_data, self.disaster_info
        )
        self.validate_one_instance_values(disasters_data, {
            'country': 'Malawi', 'day': 1,
            'glide': 'FL-1997-000025-MWI',
            'id': 4613, 'month': 2,
            'name': 'Malawi: Floods - Feb 1997',
            'status': 'past', 'type': 'Flood',
            'type_id': 'FL', 'weekday': 'Saturday',
            'year': 1997
        })

    def test_get_disasters_link(self):
        resp = self.client.get("/api/disasters_link")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        disasters_link_data = ast.literal_eval(dict_str)
        # check headers
        self.validate_count_and_pages(disasters_link_data, {
            "num_results": 2386,
            "page": 1,
            "total_pages": 239
        })
        # check content
        for o in disasters_link_data['objects']:
            self.validate_one_instance_attributes(o, self.disaster_link_info)

    def test_get_disasters_link_one(self):
        resp = self.client.get("/api/disasters_link/4613")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        disasters_link_data = ast.literal_eval(dict_str)
        # check content
        self.validate_one_instance_attributes(
            disasters_link_data, self.disaster_link_info
        )
        self.validate_one_instance_values(disasters_link_data, {
            'country_id': 146, 'id': 4613, 'org_id': 17891
        })

    def test_get_countries(self):
        resp = self.client.get("/api/countries")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        countries_data = ast.literal_eval(dict_str)
        # check headers
        self.validate_count_and_pages(countries_data, {
            "num_results": 159,
            "page": 1,
            "total_pages": 16
        })
        # check content
        for o in countries_data['objects']:
            self.validate_one_instance_attributes(o, self.country_info)

    def test_get_countries_one(self):
        resp = self.client.get("/api/countries/126")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        countries_data = ast.literal_eval(dict_str)
        # check content
        self.validate_one_instance_attributes(
            countries_data, self.country_info
        )
        self.validate_one_instance_values(countries_data, {
            'area': 301336.0, 'callingCodes': 39, 'capital': 'Rome',
            'flag': 'https://restcountries.eu/data/ita.svg', 'id': 126,
            'income': 'High income', 'income_iso2': 'XD', 'iso3': 'ITA',
            'latitude': 42.5, 'longitude': 12.65, 'name': 'Italy',
            'population': 60665600.0, 'region': 'Europe',
            'status': 'current', 'subregion': 'Southern Europe',
            'timezones': 'UTC+01:00'
        })

    def test_get_countries_link(self):
        resp = self.client.get("/api/countries_link")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        countries_link_data = ast.literal_eval(dict_str)
        # check headers
        self.validate_count_and_pages(countries_link_data, {
            "num_results": 159,
            "page": 1,
            "total_pages": 16
        })
        # check content
        for o in countries_link_data['objects']:
            self.validate_one_instance_attributes(o, self.country_link_info)

    def test_get_countries_link_one(self):
        resp = self.client.get("/api/countries_link/126")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        countries_link_data = ast.literal_eval(dict_str)
        # check content
        self.validate_one_instance_attributes(
            countries_link_data, self.country_link_info
        )
        self.validate_one_instance_values(countries_link_data, {
            'disaster_id': 5286, 'id': 126, 'org_id': 3070
        })

    def test_get_organizations(self):
        resp = self.client.get("/api/orgs")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        orgs_data = ast.literal_eval(dict_str)
        # check headers
        self.validate_count_and_pages(orgs_data, {
            "num_results": 478,
            "page": 1,
            "total_pages": 48
        })
        # check content
        for o in orgs_data['objects']:
            self.validate_one_instance_attributes(o, self.org_info)

    def test_get_organizations_one(self):
        resp = self.client.get("/api/orgs/279")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        orgs_data = ast.literal_eval(dict_str)
        # check content
        self.validate_one_instance_attributes(
            orgs_data, self.org_info
        )
        self.validate_one_instance_values(orgs_data, {
            "has_job": "No", "has_report": "Yes", "has_training": "No",
            "homepage": "http://www.pakistan.gov.pk/", "id": 279,
            "jobs": 0, 
            "logo_url": ("https://reliefweb.int/sites/reliefweb.int/"
                         "files/resources/pak_0.png"),
            "name": "Government of Pakistan", "reports": 1760,
            "s_name": "Govt. Pakistan", "status": "active",
            "trainings": 0, "type": "Government"
        })

    def test_get_organizations_link(self):
        resp = self.client.get("/api/orgs_link")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        orgs_link_data = ast.literal_eval(dict_str)
        # check headers
        self.validate_count_and_pages(orgs_link_data, {
            "num_results": 478,
            "page": 1,
            "total_pages": 48
        })
        # check content
        for o in orgs_link_data['objects']:
            self.validate_one_instance_attributes(o, self.org_link_info)

    def test_get_organizations_link_one(self):
        resp = self.client.get("/api/orgs_link/279")
        self.assertEqual(resp.status_code, 200)
        # extract the data as a dictionary
        dict_str = resp.data.decode("UTF-8")
        orgs_link_data = ast.literal_eval(dict_str)
        # check content
        self.validate_one_instance_attributes(
            orgs_link_data, self.org_link_info
        )
        self.validate_one_instance_values(orgs_link_data, {
            "country_id": 182, "disaster_id": 40159, "id": 279
        })

if __name__ == "__main__":  # pragma: no cover
    main()
