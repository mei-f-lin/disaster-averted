import requests
from flask import Flask
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from make_tables import Disaster, Country, Organization
from make_tables import Disaster_Link, Org_Link, Country_Link
from flask_cors import CORS

# Creating Flask Application
app = Flask(__name__)
CORS(app)

# Connecting MYSQL Database
# 'mysql+pymysql://{master username}:{db password}@{endpoint}/{db instance name}'
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'mysql+pymysql://disaster214'
    ':aversion469@disaster-averted-db'
    '.ct7kdzdhfbo7.us-east-1.'
    'rds.amazonaws.com/disaster_averted_mySQL_db'
)

# track modifications of objects and emit signals,
# This requires extra memory and should be disabled if not needed.
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['API_PREFIX'] = '/api'

db = SQLAlchemy(app)

# Create the Flask-Restless API manager.
manager = APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
# It actually returns a blueprint, but not actually necessary since it's
# already registered

# This creates the get API endpoints for each table
# for specifics, see the PostMan API Documentation linked
# Here, we specify max_results_per_page=-1 to allow the client to 
# specify arbitrarily large page sizes (grab all data if necessary)
PAGE_LIMIT = 10000
manager.create_api(Disaster, methods=['GET'], max_results_per_page=PAGE_LIMIT)
manager.create_api(Country, methods=['GET'], max_results_per_page=PAGE_LIMIT)
manager.create_api(Organization, methods=['GET'], max_results_per_page=PAGE_LIMIT)
manager.create_api(Disaster_Link, methods=['GET'], max_results_per_page=PAGE_LIMIT)
manager.create_api(Org_Link, methods=['GET'], max_results_per_page=PAGE_LIMIT)
manager.create_api(Country_Link, methods=['GET'], max_results_per_page=PAGE_LIMIT)

# Further endpoints relied by the frontend and AWS

# NOTE: This route is needed for the default EB health check route
@app.route('/')  
def home():
    return "ok"

@app.route('/api/query_users')
def query_users():
    # disconnect between commit metadata and gitlab user metadata, 
    # make looktable from email to gitlab username
    get_gitlab_name = {
        "mei.f.lin@utexas.edu": "mei-f-lin",
        "tkaruturi@yahoo.com": "tkaruturi",
        "tkaruturi@utexas.edu": "tkaruturi",
        "sam.su@utexas.edu": "Sam.Su1",
        "ashkanjiram@hotmail.com": "akanjiram",
        "akanjiram@ashishs-mbp.home": "akanjiram",
        "akanjiram@Ashishs-MacBook-Pro.local": "akanjiram",
        "anvithguppy@gmail.com": "anvithpotluri"
    }
    # biography, reponsibilities hard encoding
    resp = {
        "mei-f-lin": "Front-End Developer",
        "tkaruturi": "Front-End Developer",
        "Sam.Su1": "Full Stack Developer | Phase II Leader",
        "akanjiram": "Front-End Developer | Phase I Leader",
        "anvithpotluri": "Front-End Developer"
    }
    bio = {
        "mei-f-lin": "Hey I’m Mei! I’m a second year CS major interested"
        " in surgical robotics. When I’m not busy with CS, I like to boulder"
        " at ABP, play soccer, and hang out with friends. ",

        "tkaruturi": "Tejas is a Junior at UT Austin. He has some experience "
        "with web development from his previous internship. "
        "Some of his hobbies include programming and playing chess.",

        "Sam.Su1": "Yiheng is currently a junior at UT Austin and an"
        " aspiring Data Scientists. He’s responsible for implementing "
        "the backend database, API, and web application and ensures that "
        "the frontend has what it needs to display. In his spare time, "
        "Yiheng loves to hit the local arcades and play rhyme games such "
        "as Dance Dance Revolution. ",

        "akanjiram": "I am a junior computer science major at UT Austin. "
        "I am from Houston, TX, and in my free time I like to binge Netflix "
        "or hang out with my dance team.",

        "anvithpotluri": "Anvith is a sophomore at UT. He is responsible" 
        "for some of the front-end work that needs to be done. "
        "In his free time he enjoys playing basketball and watching tv."
    }
    tests = {
        "mei-f-lin": "9",
        "tkaruturi": "9",
        "Sam.Su1": "10",
        "akanjiram": "9",
        "anvithpotluri": "9"
    }
    commit_count = {}
    # extract commit information
    endpoint_commits_base = (
        'https://gitlab.com/api/v4/projects/24742557/repository/commits'
        '?per_page=100&page={page}'
    )
    # we know we have ~400 commits
    for i in range(1, 5):
        url = endpoint_commits_base.format(page=i)
        resp_commits = requests.get(url)

        for commit in resp_commits.json():
            # increment # of commits per unique gitlab name correctly
            gitlab_name = get_gitlab_name[commit['author_email']]
            if not gitlab_name in commit_count:
                commit_count[gitlab_name] = 0
            commit_count[gitlab_name] += 1

    # extract user information
    endpoint_members = 'https://gitlab.com/api/v4/projects/24742557/members'
    headers = {"Authorization": "Bearer 7EP1ebrzspAyn16eN7pE"}
    resp_members = requests.get(endpoint_members, headers=headers)

    user_table = []
    overall_stats = {'commits':0, 'issues':0}
    if resp_members.status_code != 200:
        # This means something went wrong.
        return {'Fucked up': resp_members.status_code}
    for person in resp_members.json():
        if person['access_level'] >= 40:
            # grad the issues made by this user
            user = person['username']
            endpoint_issues = f'https://gitlab.com/api/v4/projects/24742557/issues?author_username={user}'
            resp_issues = requests.get(endpoint_issues, headers=headers)

            # create the table for the user
            user_table.append({
                "name": person['name'],
                "username": user,
                "issues": len(resp_issues.json()),
                "commits": commit_count[user],
                "resp": resp[user],
                "bio": bio[user],
                "unitTests": tests[user]
            })

            # collect overall stats
            overall_stats['commits'] += commit_count[person['username']]
            overall_stats['issues'] += len(resp_issues.json())

    return {'user_table': user_table, 'overall_stats': overall_stats}

if __name__ == '__main__':
    app.run(debug=True, port=8080)
