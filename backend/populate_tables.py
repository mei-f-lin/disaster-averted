import pandas as pd
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from make_tables import Disaster, Country, Organization
from make_tables import Disaster_Link, Org_Link, Country_Link

# core interface to the database
engine = create_engine(
    'mysql+pymysql://disaster214'
    ':aversion469@disaster-averted-db'
    '.ct7kdzdhfbo7.us-east-1.'
    'rds.amazonaws.com/disaster_averted_mySQL_db')
# ORM’s “handle” to the database
Session = sessionmaker(bind=engine)
session = Session()

# we now insert into the established tables
country = pd.read_pickle("./data/final_country_table.pkl")
disaster = pd.read_pickle("./data/final_disaster_table.pkl")
org = pd.read_pickle("./data/final_org_table.pkl")

def make_disaster(row):
    return Disaster(
        id = row['disaster_id'],
        type = row['type'],
        type_id = row['type_id'],
        name = row['name'],
        glide = row['glide'],
        status = row['status'],
        year = row['year'],
        month = row['month'],
        day = row['day'],
        weekday = row['weekday'],
        country = row['country']
    )

def disaster_link(row):
    return Disaster_Link(
        id = row['disaster_id'],
        country_id = row['country_id'],
        org_id = row['org_id']
    )

def make_organization(row):
    return Organization(
        id = row['org_id'],
        type = row['type'],
        s_name = row['s_name'],
        name = row['name'],
        status = row['status'],
        homepage = row['homepage'],
        logo_url = row['logo_url'],
        has_training = row['has_training'],
        trainings = row['trainings'],
        has_report = row['has_report'],
        reports = row['reports'],
        has_job = row['has_job'],
        jobs = row['jobs']
    )

def org_link(row):
    return Org_Link(
        id = row['org_id'],
        disaster_id = row['disaster_id'],
        country_id = row['country_id']
    )

def make_country(row):
    return Country(
        id = row['country_id'],
        name = row['name'],
        longitude = row['longitude'],
        latitude = row['latitude'],
        status = row['status'],
        iso3 = row['iso3'],
        callingCodes = row['callingCodes'],
        capital = row['capital'],
        region = row['region'],
        subregion = row['subregion'],
        population = row['population'],
        area = row['area'],
        timezones = row['timezones'],
        flag = row['flag'],
        income = row['income'],
        income_iso2 = row['income_iso2']
    )

def country_link(row):
    return Country_Link(
        id = row['country_id'],
        disaster_id = row['disaster_id'],
        org_id = row['org_id']
    )

# add the model tables first
session.add_all(
    country.apply(make_country, axis=1).to_list()
)

session.add_all(
    disaster.apply(make_disaster, axis=1).to_list()
)

session.add_all(
    org.apply(make_organization, axis=1).to_list()
)

session.commit()

# then, add the model linking tables
session.add_all(
    country.apply(country_link, axis=1).to_list()
)

session.add_all(
    disaster.apply(disaster_link, axis=1).to_list()
)

session.add_all(
    org.apply(org_link, axis=1).to_list()
)

session.commit()
session.close()
